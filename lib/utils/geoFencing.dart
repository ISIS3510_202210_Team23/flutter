import 'dart:math' as math;

import 'package:location/location.dart';

class GeoFencing{

  final Location _location = Location();

  bool? before;

  bool _serviceEnabled = false;

  PermissionStatus? _permissionGranted;

  GeoFencing();

  Location getlocObject(){
    return _location;
  }

  setup() async{
    _serviceEnabled = await _location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await _location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }
    _permissionGranted = await _location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await _location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _location.changeSettings(accuracy: LocationAccuracy.balanced, interval: 5000, distanceFilter: 0);
    _location.enableBackgroundMode(enable: true);
    
  }

  Stream<LocationData>? getStream(){
    try{
      return _location.onLocationChanged;
    }
    catch(e){
      return null;
    }
  }

  double? calculateDistance(List<double>homeL, List<double> currentLoc){
    try{
      double? latC = currentLoc[0];
      double? lngC = currentLoc[1];
      var p = 0.017453292519943295;
      double difLat = latC - homeL[0];
      difLat = difLat*p;
      
      double difLng = lngC-homeL[1];
      difLng = difLng*p;

      double internalSqrt = math.sqrt(math.pow(math.sin((difLat)/2), 2) 
                          + (math.cos(homeL[0]) 
                          * math.cos(latC) 
                          * math.pow(math.sin((difLng)/2), 2)));

      double distance = 2*6371000*math.asin(internalSqrt);
      double minDistance = 10;
      if(before == null){
        if(distance < minDistance){
          before = true;
          return null;
        }
        else{
          before = false;
          return null;
        }
      }
      else{
        if(!before! && distance < minDistance){
          before = true;
          return distance;
        }
        else if(before! && distance > minDistance){
          before = false;
          return null;
        }
        else{
          return null;
        }
      }

    }
    catch(e){
      return null;
    }
  }

}