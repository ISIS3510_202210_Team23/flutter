import 'package:intl/intl.dart';

String formatMoney(double amount) {
  final numberFormat = NumberFormat("#,##0.00", "en_US");
  return numberFormat.format(amount);
}
