import 'package:flutter_local_notifications/flutter_local_notifications.dart';
class NotificationService {

  static final NotificationService _notificationService = NotificationService._internal();

  factory NotificationService(){
    return _notificationService;
  }

  NotificationService._internal();

  final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin = FlutterLocalNotificationsPlugin();

  Future<void> initNotification() async {
    final AndroidInitializationSettings initializationSettingsAndroid = AndroidInitializationSettings('@mipmap/ic_launcher');

    final InitializationSettings initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
    );
    await flutterLocalNotificationsPlugin.initialize(initializationSettings, onSelectNotification: selectNotif);
  }

  void selectNotif(String? value) async{
      // print("Yeet");
    }

  Future<void> showNotification(id, title, body) async{
    AndroidNotificationDetails _androidNotificationDetails = const
      AndroidNotificationDetails(
        'Main-Channel',
        'Main-Channel',
        playSound: true,
        priority: Priority.high,
        importance: Importance.high,
  );

    await flutterLocalNotificationsPlugin.show(id,title, body,
      NotificationDetails(
        android: _androidNotificationDetails
      )
    );
  }
}