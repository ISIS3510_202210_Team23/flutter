import 'dart:async';
import 'dart:collection';

import 'package:camera/camera.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:location/location.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/registerCubit.dart';
import 'package:lodge/data/cubits/user_mgt_cubit.dart';
import 'package:lodge/data/db/local_storage_sql_lite.dart';
import 'package:lodge/data/repositories/complex_repository.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/repositories/gps_stats_repository.dart';
import 'package:lodge/screens/financial_info/financial_info.dart';
import 'package:lodge/screens/login/login.dart';
import 'package:lodge/screens/payment/payment.dart';
import 'package:lodge/screens/register/codeCreate.dart';
import 'package:lodge/screens/register/register.dart';
import 'package:lodge/screens/residential_complex/take_picture.dart';
import 'package:lodge/screens/tabs/tabScreen.dart';
import 'package:lodge/utils/geoFencing.dart';
import 'package:lodge/utils/notification_service.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await LocalStorageSQLite().database;
  await Firebase.initializeApp();
  ConnectivityResult initalConn = await Connectivity().checkConnectivity();
  runApp(MyApp(
    connResult: initalConn,
  ));
}

class MyApp extends StatelessWidget {
  final ConnectivityResult connResult;

  const MyApp({Key? key, required this.connResult}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ConnectivityCubit>(
            create: (context) => ConnectivityCubit(initial: connResult)),
        BlocProvider<UserMgtCubit>(
            create: (context) => UserMgtCubit(
                connectionCubit: BlocProvider.of<ConnectivityCubit>(context))),
        BlocProvider<RegisterCubit>(
            create: (context) => RegisterCubit(
                connectionCubit: BlocProvider.of<ConnectivityCubit>(context))),
      ],
      child: const MainApp(),
    );
  }
}

class MainApp extends StatefulWidget {
  const MainApp({Key? key}) : super(key: key);

  @override
  State<MainApp> createState() => _MainAppState();
}

class _MainAppState extends State<MainApp> {
  GeoFencing geo = GeoFencing();
  ComplexRepository repo = ComplexRepository();
  GpsStatsRepository gpsRepo = GpsStatsRepository();

  StreamSubscription<LocationData>? _streamSubscription;

  final _navigatorKey = GlobalKey<NavigatorState>();
  Map<String, dynamic> _geoData = {
    "latitude": "0",
    "longitude": "0",
    "inside": "false"
  };

  Future<void> geoListen() async {
    final servAvail = await geo.getlocObject().serviceEnabled();
    final permAvail = await geo.getlocObject().hasPermission();

    if (!servAvail ||
        permAvail == PermissionStatus.denied ||
        permAvail == PermissionStatus.deniedForever) {
      return;
    }
    List<double>? homeL = await repo.getComplexLocationData();
    if (homeL != null) {
      _streamSubscription =
          geo.getStream()?.skip(1).listen((LocationData locationData) {
        double? distance = geo.calculateDistance(
            homeL, [locationData.latitude ?? 0, locationData.longitude ?? 0]);
        setState(() {
          _geoData = {
            "latitude": locationData.latitude.toString(),
            "longitude": locationData.longitude.toString(),
            "inside": geo.before.toString()
          };
        });
        if (distance != null) {
          NotificationService()
              .showNotification(0, "Lodge", "Check out today's activity!");
          gpsRepo.recordTime();
        }
      });
    }
  }

  void cancelStreamSub() {
    _streamSubscription?.cancel();
  }

  @override
  void initState() {
    NotificationService().initNotification();
    geo.setup();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        brightness: Brightness.light,
        textTheme: GoogleFonts.mPlus1TextTheme(),
        primaryColor: const Color(0xFF3378cc),
        secondaryHeaderColor: const Color(0xFF005eb1),
      ),
      navigatorKey: _navigatorKey,
      home: StreamBuilder<User?>(
          stream: FirebaseAuth.instance.authStateChanges(),
          builder: ((context, snap) {
            if (snap.data != null) {
              context
                  .read<UserMgtCubit>()
                  .setUser(FirebaseAuth.instance.currentUser);
              geoListen();
              return TabsWrapper(geoData: _geoData,);
            } else {
              cancelStreamSub();
              return const LoginScreen();
            }
          })),
      routes: <String, WidgetBuilder>{
        '/login': (BuildContext context) => const LoginScreen(),
        '/register': (BuildContext context) => const RegisterScreen(),
        '/registerUser': (BuildContext context) => const ComplexCodeScreen(),
        '/registerComplex': (BuildContext context) =>
            const CreateComplexScreen(),
        '/tabs': (BuildContext context) => TabsWrapper(geoData: _geoData,),
        '/payment': (BuildContext context) => Payment(
            bill: ModalRoute.of(context)?.settings.arguments as BillModel),
        '/takePicture': (BuildContext context) => TakePictureResidentialComplex(
              camera: (ModalRoute.of(context)?.settings.arguments
                  as LinkedHashMap)["camera"] as CameraDescription,
              treeContext: (ModalRoute.of(context)?.settings.arguments
                  as LinkedHashMap)["treeContext"] as BuildContext,
            ),
        '/financialInfo': (BuildContext context) => const FinancialInfo()
      },
    );
  }
}
