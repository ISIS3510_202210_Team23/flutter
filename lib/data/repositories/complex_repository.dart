import 'dart:io';

import 'package:lodge/data/dao/firestore/firestore_residential_complex.dart';
import 'package:lodge/data/dao/local_storage/local_storage_complex.dart';
import 'package:lodge/data/models/complexModel.dart';

class ComplexRepository {
  final FirestoreResidentialComplex _firestoreComplex =
      FirestoreResidentialComplex();
  final LocalStorageComplex _localStorageComplex = LocalStorageComplex();

  Future<ComplexModel> get(String id) async {
    try {
      ComplexModel complex = await _firestoreComplex.get(id);
      return complex;
    } catch (e) {
      throw Exception("Error getting complex");
    }
  }

  Future<ComplexModel> getLocalResidentialComplexData() async {
    try {
      ComplexModel complex = await _localStorageComplex.get(null);
      return complex;
    } catch (e) {
      throw Exception("Empty");
    }
  }

  Future<bool> post(ComplexModel newObject) async {
    try {
      await _firestoreComplex.post(newObject);
      return true;
    } catch (e) {
      return false;
    }
  }

  Future<List<double>?>? getComplexLocationData() {
    return _firestoreComplex.getComplexLocationData();
  }

  Future<String> postBanner(ComplexModel complex, String path) async {
    Map<String, dynamic> mapPaths = await _firestoreComplex.postBanner(path);
    await _firestoreComplex.update(complex.copyWith(image: mapPaths["path"]));
    return mapPaths["downloadImage"];
  }
}
