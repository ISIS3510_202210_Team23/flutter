import 'package:lodge/data/dao/firestore/firestore_gps_stats.dart';

class GpsStatsRepository {
  final FirestoreGpsStats _firestoreGpsStats = FirestoreGpsStats();

  Future<bool> recordTime() async {
    return await _firestoreGpsStats.recordTime();
  }
}