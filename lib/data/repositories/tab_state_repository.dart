import 'package:lodge/data/dao/firestore/firestore_tabs_stats.dart';
import 'package:lodge/data/models/tab_stats_model.dart';

class TabRepository{
  final FirestoreTabs _firestoreTabs = FirestoreTabs();

  void updateStats(TabStatModel model){
    _firestoreTabs.post(model);
  }
}