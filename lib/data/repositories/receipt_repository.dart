import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lodge/data/dao/firestore/firestore_receipt.dart';
import 'package:lodge/data/dao/local_storage/local_storage_receipt.dart';
import 'package:lodge/data/models/receipt.dart';

class ReceiptRepository {
  final FirestoreReceipt _firestoreReceipt = FirestoreReceipt();
  final LocalStorageReceipt _localStorageReceipt = LocalStorageReceipt();

  Future<List<ReceiptModel>> getAllFirestore(String id, int limit) async {
    DocumentSnapshot snapshot = await _firestoreReceipt.getDoc(id);
    return await _firestoreReceipt.getAll(snapshot, limit);
  }

  Future<List<ReceiptModel>> getAllLocalStorage(int offset, int limit) async {
    return await _localStorageReceipt.getAll(offset, limit);
  }

  Future<bool> postFirestore(ReceiptModel receipt) async {
    return _firestoreReceipt.post(receipt);
  }

  Future<List<ReceiptModel>> getAllFirestoreStoreLocal(int limit) async {
    final receipts = await _firestoreReceipt.getAll(null, limit);

    await _localStorageReceipt.deleteTable();
    _localStorageReceipt.postList(receipts);

    return receipts;
  }
}
