import 'package:lodge/data/dao/firestore/firestore_financial_info.dart';
import 'package:lodge/data/dao/local_storage/local_storage_financial_info.dart';
import 'package:lodge/data/models/financial_info_model.dart';

class FinancialInfoRepository {
  final FirestoreFinancialInfo _firestoreFinancialInfo =
      FirestoreFinancialInfo();

  final LocalStorageFinancialInfo _localStorageFinancialInfo =
      LocalStorageFinancialInfo();

  Future<FinancialInfoModel> getFirestoreFinancialInfo() async {
    int totalBills = await _firestoreFinancialInfo.getTotalBills();
    int billsPaid = await _firestoreFinancialInfo.getBillsPaid();

    _localStorageFinancialInfo.postBasicInfo(totalBills, billsPaid);
    return FinancialInfoModel(totalBills: totalBills, billsPaid: billsPaid);
  }

  Future<FinancialInfoModel> getLocalFinancialInfo() async {
    int totalBills = await _localStorageFinancialInfo.getTotalBills();
    int billsPaid = await _localStorageFinancialInfo.getBillsPaid();

    return FinancialInfoModel(totalBills: totalBills, billsPaid: billsPaid);
  }
}
