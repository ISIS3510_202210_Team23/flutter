import 'package:lodge/data/dao/firestore/firestore_user.dart';
import 'package:lodge/data/models/residentModel.dart';

class UserRepository{
  final FirestoreUser _firestoreUser = FirestoreUser();

  Future<ResidentModel> getUser(id) async{
    try{
      ResidentModel user = await _firestoreUser.get(id);
      return user;
    }
    catch(e){
      throw Exception("Error getting user");
    }
  }
}