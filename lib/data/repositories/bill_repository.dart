import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lodge/data/dao/firestore/firestore_bill.dart';
import 'package:lodge/data/dao/local_storage/local_storage_bill.dart';
import 'package:lodge/data/models/bill.dart';

class BillRepository {
  final FirestoreBill _firestoreBill = FirestoreBill();
  final LocalStorageBill _localStorageBill = LocalStorageBill();

  Future<List<BillModel>> getAllFirestore(String id, int limit) async {
    DocumentSnapshot snapshot = await _firestoreBill.getDoc(id);
    return _firestoreBill.getAll(snapshot, limit);
  }

  Future<List<BillModel>> getAllLocalStorage(int offset, int limit) async {
    return _localStorageBill.getAll(offset, limit);
  }

  Future<List<BillModel>> getAllFirestoreStoreLocal(int limit) async {
    final bills = await _firestoreBill.getAll(null, limit);

    await _localStorageBill.deleteTable();
    _localStorageBill.postList(bills);

    return bills;
  }
}
