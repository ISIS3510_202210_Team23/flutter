import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lodge/data/models/payment_option.dart';
import 'package:lodge/data/dao/dao_interface.dart';

class PaymentOptionRepository implements DaoInterface<PaymentOptionModel> {
  final FirebaseFirestore _storage = FirebaseFirestore.instance;

  @override
  Future<List<PaymentOptionModel>> getAll(offset, limit) async {
    final paymentOptionsData = await _storage.collection("PaymentOption").get();
    final List<PaymentOptionModel> paymentsOptions =
        paymentOptionsData.docs.map((e) {
      final paymentOption = e.data();
      return PaymentOptionModel(
          name: paymentOption["name"],
          description: paymentOption["description"],
          icon: paymentOption["icon"]);
    }).toList();

    return paymentsOptions;
  }

  @override
  Future<bool> delete(PaymentOptionModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<PaymentOptionModel> get(String id) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<bool> post(PaymentOptionModel newObject) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future<bool> update(PaymentOptionModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }
}
