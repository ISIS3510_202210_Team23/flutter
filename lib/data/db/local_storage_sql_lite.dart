import 'package:path/path.dart';
import 'package:sqflite/sqflite.dart';

class LocalStorageSQLite {
  static final LocalStorageSQLite _singleton = LocalStorageSQLite._internal();

  static Database? _db;

  factory LocalStorageSQLite() {
    return _singleton;
  }

  LocalStorageSQLite._internal();

  Future<Database> get database async {
    if (_db != null) {
      return _db!;
    }
    _db = await _initDatabase("lodge.db");
    return _db!;
  }

  Future<Database> _initDatabase(String filepath) async {
    final dbPath = await getDatabasesPath();
    final path = join(dbPath, filepath);

    return await openDatabase(path, version: 1, onCreate: _onCreate);
  }

  Future _onCreate(Database db, int version) async {
    final batch = db.batch();
    // Bill
    var id = "TEXT PRIMARY KEY";
    var name = "TEXT NOT NULL";
    var amount = "REAL NOT NULL";
    var dueDate = "TEXT NOT NULL";
    var publishedDate = "TEXT NOT NULL";
    var paid = "INTEGER NOT NULL";

    var sql = '''
      CREATE TABLE Bill (
        id $id,
        name $name,
        amount $amount,
        dueDate $dueDate,
        publishDate $publishedDate,
        paid $paid
      );
    ''';
    batch.execute(sql);

    // Receipt
    id = "TEXT PRIMARY KEY";
    var date = "TEXT NOT NULL";
    var bill = "TEXT NOT NULL";
    var paymentOption = "TEXT NOT NULL";

    sql = '''
      CREATE TABLE Receipt (
        id $id,
        date $date,
        bill $bill,
        paymentOption $paymentOption
      );
    ''';
    batch.execute(sql);

    await batch.commit();
  }
}
