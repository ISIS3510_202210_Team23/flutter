import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/models/payment_option.dart';
import 'package:lodge/data/models/receipt.dart';

class FirestoreReceipt implements DaoInterface<ReceiptModel> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _storage = FirebaseFirestore.instance;

  @override
  Future<bool> delete(ReceiptModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<ReceiptModel> get(String id) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<List<ReceiptModel>> getAll(offset, limit) async {
    String? id = _auth.currentUser?.uid;

    //GET Receipts by User
    var query = _storage
        .collection("Receipts")
        .where("user", isEqualTo: id)
        .orderBy("date", descending: true)
        .limit(limit);
    if (offset != null) query = query.startAfterDocument(offset);
    final receiptsData = await query.get();

    final List<ReceiptModel> receipts = receiptsData.docs.map((e) {
      final receipt = e.data();
      return ReceiptModel(
          id: e.id,
          bill: BillModel(
            id: receipt["bill"]["id"],
            name: receipt["bill"]["name"],
            amount: receipt["bill"]["amount"].toDouble(),
            dueDate: receipt["bill"]["dueDate"],
            publishDate: receipt["bill"]["publishDate"],
            paid: receipt["bill"]["paid"],
          ),
          date: receipt["date"],
          paymentOption: PaymentOptionModel(
              name: receipt["paymentOption"]["name"],
              description: receipt["paymentOption"]["description"],
              icon: receipt["paymentOption"]["icon"] ?? 0));
    }).toList();

    return receipts;
  }

  @override
  Future<bool> post(ReceiptModel newObject) async {
    String? id = _auth.currentUser?.uid;

    //GET User
    final docUser = await _storage.collection("Users").doc(id).get();
    final userData = docUser.data();

    final mapReceipt = newObject.toMap();
    mapReceipt["house"] = userData?["house"];
    mapReceipt["user"] = docUser.id;

    mapReceipt["bill"]["complex"] = userData?["complex"];
    mapReceipt["bill"]["paid"] = true;

    try {
      await _storage.collection("Receipts").add(mapReceipt);
    } catch (e) {
      return false;
    }

    return true;
  }

  @override
  Future<bool> update(ReceiptModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }

  Future<DocumentSnapshot> getDoc(String id) async {
    return await _storage.collection("Receipts").doc(id).get();
  }
}
