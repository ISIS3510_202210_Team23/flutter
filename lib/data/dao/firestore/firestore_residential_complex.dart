import 'dart:convert';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:lodge/data/models/complexModel.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/models/residentModel.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:firebase_storage/firebase_storage.dart';

class FirestoreResidentialComplex implements DaoInterface<ComplexModel> {
  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final FirebaseStorage _cloudStorage = FirebaseStorage.instance;
  SharedPreferences? sharedPrefs;

  @override
  Future<bool> delete(ComplexModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<ComplexModel> get(String id) async {
    sharedPrefs ??= await SharedPreferences.getInstance();
    try {
      final complexDoc =
          await _firestore.collection("ResidentialComplex").doc(id).get();
      final complexData = complexDoc.data();
      if (complexData == null || complexData.isEmpty) {
        throw Exception();
      }

      ResidentModel? admin;

      try {
        final adminQuery = await _firestore
            .collection("Users")
            .limit(1)
            .where('isAdmin', isEqualTo: true)
            .where('complex', isEqualTo: id)
            .get();
        final adminData = adminQuery.docs[0].data();
        admin = ResidentModel(
            name: adminData['name'],
            phone: adminData['phone'],
            house: adminData['house'],
            id: adminQuery.docs[0].reference.id,
            admin: true,
            email: adminData["email"],
            complex: adminData["complex"],
            image: adminData["userImage"]??"");
      } catch (e) {
        admin = null;
      }
      final imageUrl = complexData["image"] == ""
          ? ""
          : await _cloudStorage
              .ref()
              .child(complexData["image"])
              .getDownloadURL();

      final complex = ComplexModel(
          id: complexDoc.id,
          name: complexData["name"],
          city: complexData["city"],
          address: complexData["address"],
          image: imageUrl,
          geoLocation: complexData["location"],
          admin: admin);
      await sharedPrefs?.setString("Complex", json.encode(complex.toMap()));
      return complex;
    } catch (e) {
      final String? complexLocalString = sharedPrefs?.getString("Complex");
      if (complexLocalString != null) {
        final complexMap = json.decode(complexLocalString);
        return ComplexModel.fromMap(complexMap);
      }
      throw Exception("Could not get residential complex with id: " + id);
    }
  }

  @override
  Future<List<ComplexModel>> getAll(offset, limit) {
    // TODO: implement getAll
    throw UnimplementedError();
  }

  @override
  Future<bool> post(ComplexModel newObject) async {
    final map = newObject.toMap();
    map["image"] = "";
    map["location"] = const GeoPoint(0, 0);
    map["houses"] = [];
    final admin = map["admin"];
    map.remove("admin");
    map.remove("geoLocation");

    final added = await _firestore.collection('ResidentialComplex').add(map);
    await _firestore
        .collection('Users')
        .doc(admin["id"])
        .update({"complex": added.id});

    return true;
  }

  @override
  Future<bool> update(ComplexModel updateObject) async {
    try {
      var map = updateObject.toMap();
      map.remove("admin");
      await _firestore
          .collection("ResidentialComplex")
          .doc(updateObject.id)
          .update(map);
    } catch (e) {
      return false;
    }
    return true;
  }

  Future<List<double>?>? getComplexLocationData() async {
    if (_firebaseAuth.currentUser != null) {
      final userDataQuery = await _firestore
          .collection('Users')
          .doc(_firebaseAuth.currentUser?.uid)
          .get();
      final userData = userDataQuery.data();
      final complexId = userData?["complex"];
      final complexQuery = await _firestore
          .collection('ResidentialComplex')
          .doc(complexId)
          .get();

      final complexData = complexQuery.data();
      if (complexData != null) {
        GeoPoint location = complexData["location"];
        return [location.latitude, location.longitude];
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  Future<Map<String, dynamic>> postBanner(String path) async {
    final ref = _cloudStorage.ref("ResidentialComplex/${basename(path)}");
    File file = File(path);
    await ref.putFile(file);

    String downloadImage =
        await _cloudStorage.ref().child(ref.fullPath).getDownloadURL();
    file.delete();
    return {"downloadImage": downloadImage, "path": ref.fullPath};
  }
}
