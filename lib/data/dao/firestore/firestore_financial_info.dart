import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirestoreFinancialInfo {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _storage = FirebaseFirestore.instance;

  Future<int> getTotalBills() async {
    String? id = _auth.currentUser?.uid;
    //GET User
    final docUser = await _storage.collection("Users").doc(id).get();
    final userData = docUser.data();

    final billsData = await _storage
        .collection('Bills')
        .where("complex", isEqualTo: userData?["complex"])
        .get();

    return billsData.docs.length;
  }

  Future<int> getBillsPaid() async {
    String? id = _auth.currentUser?.uid;
    //GET User
    final docUser = await _storage.collection("Users").doc(id).get();
    final userData = docUser.data();

    final billsData = await _storage
        .collection('Bills')
        .where("complex", isEqualTo: userData?["complex"])
        .where("paid", isEqualTo: true)
        .get();

    return billsData.docs.length;
  }
}
