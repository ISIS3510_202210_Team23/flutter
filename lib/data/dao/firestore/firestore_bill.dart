import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/models/bill.dart';

class FirestoreBill implements DaoInterface<BillModel> {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _storage = FirebaseFirestore.instance;

  @override
  Future<bool> delete(BillModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<BillModel> get(String id) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<List<BillModel>> getAll(offset, limit) async {
    String? id = _auth.currentUser?.uid;
    //GET User
    final docUser = await _storage.collection("Users").doc(id).get();
    final userData = docUser.data();

    //GET Bills by House
    var query = _storage
        .collection("Bills")
        .where("house", isEqualTo: userData?["house"])
        .where("paid", isEqualTo: false)
        .orderBy("publishDate", descending: true)
        .limit(limit);
    if (offset != null) query = query.startAfterDocument(offset);
    final billsData = await query.get();

    var index = 0;
    List<BillModel> bills = [];
    Map<String, dynamic> item;
    while (index < billsData.docs.length) {
      item = billsData.docs[index].data();
      bills.add(BillModel(
          id: billsData.docs[index].id,
          name: item["name"],
          amount: item["amount"].toDouble(),
          dueDate: item["dueDate"],
          publishDate: item["publishDate"],
          paid: item["paid"] == 0 ? false : true));
      index++;
    }

    // final List<BillModel> bills = billsData.docs.map((e) {
    //   final bill = e.data();
    //   return BillModel(
    //       id: e.id,
    //       name: bill["name"],
    //       amount: bill["amount"].toDouble(),
    //       dueDate: bill["dueDate"],
    //       publishDate: bill["publishDate"],
    //       paid: bill["paid"] == 0 ? false : true);
    // }).toList();

    return bills;
  }

  @override
  Future<bool> post(BillModel newObject) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future<bool> update(BillModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }

  Future<DocumentSnapshot> getDoc(String id) {
    return _storage.collection("Bills").doc(id).get();
  }
}
