import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/dao/local_storage/local_storage_profile.dart';
import 'package:lodge/data/models/residentModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FirestoreUser implements DaoInterface<ResidentModel>{

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;
  final FirebaseStorage _firebaseStorage = FirebaseStorage.instance;
  final LocalStorageProfile _localStorageProfile = LocalStorageProfile();

  @override
  Future<bool> delete(ResidentModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<ResidentModel> get(String id) async{
    
    final userDoc = await _firestore.collection('Users').doc(id).get();
    final data = userDoc.data();

    if(data != null){
      var imageUrl = "";
      if(data["userImage"] != null){
        imageUrl = await _firebaseStorage.ref().child("UserProfile/${data["userImage"]}").getDownloadURL();
      }
      ResidentModel resident = ResidentModel(id: userDoc.id, name: data["name"], phone: data["phone"], house: data["house"], admin: data["isAdmin"], email: data["email"], complex: data["complex"], image: imageUrl);
      SharedPreferences.getInstance().then((sharedPrefs){
        _localStorageProfile.setProfileInfo(resident);
        // sharedPrefs.setBool("ResidentIsAdmin", resident.admin);
        // sharedPrefs.setString("ResidentName", resident.name);
        // sharedPrefs.setString("ResidentHouse", resident.house);
        // sharedPrefs.setString("ResidentEmail", resident.email);
        // sharedPrefs.setString("ResidentPhone", resident.phone);
        // sharedPrefs.setString("ResidentId", resident.id);
        // sharedPrefs.setString("ResidentImage", resident.image);
      });
      return resident;
    }
    else{
      throw Exception("Error getting user");
    }
  }

  @override
  Future<List<ResidentModel>> getAll(offset, limit) {
    // TODO: implement getAll
    throw UnimplementedError();
  }

  @override
  Future<bool> post(ResidentModel newObject) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future<bool> update(ResidentModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }
  
}