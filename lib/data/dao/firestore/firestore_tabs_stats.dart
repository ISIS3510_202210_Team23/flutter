import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/models/tab_stats_model.dart';

class FirestoreTabs implements DaoInterface<TabStatModel>{

  final FirebaseFirestore _firestore = FirebaseFirestore.instance;

  @override
  Future<bool> delete(TabStatModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<TabStatModel> get(String id) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<List<TabStatModel>> getAll(offset, limit) {
    // TODO: implement getAll
    throw UnimplementedError();
  }

  @override
  Future<bool> post(TabStatModel newObject) async{
    _firestore.collection('Stats').doc('statsTime').update({
      "${newObject.name}.count": FieldValue.increment(1),
      "${newObject.name}.time": FieldValue.increment(newObject.time)
    });
    return true;
  }

  @override
  Future<bool> update(TabStatModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }
  
}