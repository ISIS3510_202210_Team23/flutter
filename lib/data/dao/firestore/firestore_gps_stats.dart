import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirestoreGpsStats {
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _storage = FirebaseFirestore.instance;

  Future<bool> recordTime() async {
    String? id = _auth.currentUser?.uid;
    //GET User
    final docUser = await _storage.collection("Users").doc(id).get();
    final userData = docUser.data();

    //GET Complex
    final docComplex =
        await _storage.collection("ResidentialComplex").doc(userData?["complex"]).get();
    final complexData = docComplex.data();

    //POST current time
    final now = DateTime.now();
    _storage.collection("Stats").doc("statsGps").update(
        {"${complexData?["name"]}.${now.hour}": FieldValue.increment(1)});

    return true;
  }
}
