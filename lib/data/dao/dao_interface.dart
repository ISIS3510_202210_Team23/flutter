abstract class DaoInterface<T> {
  Future<List<T>> getAll(offset, limit);

  Future<T> get(String id);

  Future<bool> post(T newObject);

  Future<bool> update(T updateObject);

  Future<bool> delete(T deleteObject);
}
