import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lodge/data/db/local_storage_sql_lite.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:sqflite/sqflite.dart';

class LocalStorageBill implements DaoInterface<BillModel> {
  final LocalStorageSQLite _localStorageSQLite = LocalStorageSQLite();

  @override
  Future<bool> delete(BillModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<BillModel> get(String id) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<List<BillModel>> getAll(offset, limit) async {
    final db = await _localStorageSQLite.database;

    final List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM Bill LIMIT $limit OFFSET $offset");
    return List.generate(
        maps.length,
        (index) => BillModel(
              id: maps[index]['id'],
              name: maps[index]['name'],
              amount: maps[index]['amount'],
              dueDate:
                  Timestamp.fromDate(DateTime.parse(maps[index]['dueDate'])),
              publishDate: Timestamp.fromDate(
                  DateTime.parse(maps[index]['publishDate'])),
              paid: maps[index]['paid'] == 1 ? true : false,
            ));
  }

  @override
  Future<bool> post(BillModel newObject) async {
    final db = await _localStorageSQLite.database;
    final map = {
      ...newObject.toMap(),
      'dueDate': newObject.dueDate.toDate().toString(),
      'publishDate': newObject.publishDate.toDate().toString(),
    };

    final res = await db.insert("Bill", map,
        conflictAlgorithm: ConflictAlgorithm.replace);
    return res != 0;
  }

  @override
  Future<bool> update(BillModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }

  Future<bool> postList(List<BillModel> newObjects) async {
    final db = await _localStorageSQLite.database;
    final batch = db.batch();

    Map<String, dynamic> map;
    for (var i = 0; i < newObjects.length; i++) {
      map = newObjects[i].toMap();
      map['dueDate'] = newObjects[i].dueDate.toDate().toString();
      map["publishDate"] = newObjects[i].publishDate.toDate().toString();
      map["paid"] = newObjects[i].paid ? 1 : 0;

      batch.insert("Bill", map, conflictAlgorithm: ConflictAlgorithm.replace);
    }

    await batch.commit();

    return true;
  }

  Future<bool> deleteTable() async {
    final db = await _localStorageSQLite.database;
    await db.delete("Bill");
    return true;
  }
}
