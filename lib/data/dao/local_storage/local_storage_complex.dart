import 'dart:convert';

import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/models/complexModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageComplex implements DaoInterface<ComplexModel> {
  @override
  Future<bool> delete(ComplexModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<ComplexModel> get(String? id) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    final String? complexString = sharedPrefs.getString("Complex");
    if(complexString != null){
      return ComplexModel.fromMap(jsonDecode(complexString));
    }
    else{
      throw Exception("Empty");
    }
  }

  Future<Map<String, dynamic>?> getExtraInfo() async{
    final sharedPrefs = await SharedPreferences.getInstance();
    final String? nameString = sharedPrefs.getString("name");
    final String? houseString = sharedPrefs.getString("house");
    if(nameString != null && houseString != null){
      return {"name": nameString, "houseString": houseString};
    }
    else{
      return null;
    }
  }

  Future<bool> postExtraInfo(Map<String, String> data) async{
    final sharedPrefs = await SharedPreferences.getInstance();
    try{
      data.forEach((key, value) async {
        await sharedPrefs.setString(key, value);
      });
      return true;
    }
    catch(e){
      return false;
    }
  }

  @override
  Future<List<ComplexModel>> getAll(offset, limit) {
    // TODO: implement getAll
    throw UnimplementedError();
  }

  @override
  Future<bool> post(ComplexModel newObject) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future<bool> update(ComplexModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }

}