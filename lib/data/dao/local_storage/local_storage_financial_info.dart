import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageFinancialInfo {
  Future<int> getTotalBills() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    return sharedPrefs.getInt("totalBills") ?? 0;
  }

  Future<int> getBillsPaid() async {
    final sharedPrefs = await SharedPreferences.getInstance();
    return sharedPrefs.getInt("billsPaid") ?? 0;
  }

  Future<bool> postBasicInfo(int totalBills, int billsPaid) async {
    final sharedPrefs = await SharedPreferences.getInstance();
    try {
      await sharedPrefs.setInt("totalBills", totalBills);
      await sharedPrefs.setInt("billsPaid", billsPaid);
    } catch (e) {
      return false;
    }
    return true;
  }
}
