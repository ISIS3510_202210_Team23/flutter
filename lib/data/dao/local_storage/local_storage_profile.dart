import 'dart:convert';

import 'package:lodge/data/models/residentModel.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LocalStorageProfile{

  Future<ResidentModel> getProfileInfo() async{
    final sharedPrefs = await SharedPreferences.getInstance();
    final jsonString = sharedPrefs.getString("BasicInfo");
    if(jsonString == null){
      throw Exception("Error retreiving info, please check your internet connection");
    }
    final Map<String, dynamic> localInfo = json.decode(jsonString);
    if(jsonString != ""){
      return ResidentModel.fromMap(localInfo);
    }
    else{
      throw Exception("Error retreiving info, please check your internet connection");
    }
  }

  Future<void> setProfileInfo(ResidentModel model) async{
    final sharedPrefs = await SharedPreferences.getInstance();
    sharedPrefs.setString("BasicInfo", json.encode(model.toMap()));
  }
}