import 'dart:convert';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:lodge/data/db/local_storage_sql_lite.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/models/receipt.dart';
import 'package:sqflite/sqflite.dart';

class LocalStorageReceipt implements DaoInterface<ReceiptModel> {
  final LocalStorageSQLite _localStorageSQLite = LocalStorageSQLite();

  @override
  Future<bool> delete(ReceiptModel deleteObject) {
    // TODO: implement delete
    throw UnimplementedError();
  }

  @override
  Future<ReceiptModel> get(String id) {
    // TODO: implement get
    throw UnimplementedError();
  }

  @override
  Future<List<ReceiptModel>> getAll(offset, limit) async {
    final db = await _localStorageSQLite.database;

    List<Map<String, dynamic>> maps =
        await db.rawQuery("SELECT * FROM Receipt LIMIT $limit OFFSET $offset");

    Map<String, dynamic> receiptMap;
    return List.generate(maps.length, (index) {
      receiptMap = {
        ...maps[index],
        "bill": jsonDecode(maps[index]["bill"]),
        "paymentOption": jsonDecode(maps[index]["paymentOption"])
      };
      receiptMap["date"] =
          Timestamp.fromDate(DateTime.parse(receiptMap["date"]));
      receiptMap["bill"]["dueDate"] =
          Timestamp.fromDate(DateTime.parse(receiptMap["bill"]["dueDate"]));
      receiptMap["bill"]["publishDate"] =
          Timestamp.fromDate(DateTime.parse(receiptMap["bill"]["publishDate"]));

      return ReceiptModel.fromMap(receiptMap);
    });
  }

  @override
  Future<bool> post(ReceiptModel newObject) {
    // TODO: implement post
    throw UnimplementedError();
  }

  @override
  Future<bool> update(ReceiptModel updateObject) {
    // TODO: implement update
    throw UnimplementedError();
  }

  Future<bool> postList(List<ReceiptModel> newObjects) async {
    final db = await _localStorageSQLite.database;
    final batch = db.batch();

    Map<String, dynamic> map;
    for (var i = 0; i < newObjects.length; i++) {
      map = newObjects[i].toMap();
      map["date"] = map["date"].toDate().toString();
      map["bill"]["dueDate"] = map["bill"]["dueDate"].toDate().toString();
      map["bill"]["publishDate"] =
          map["bill"]["publishDate"].toDate().toString();

      map["bill"] = jsonEncode(map["bill"]);
      map["paymentOption"] = jsonEncode(map["paymentOption"]);

      batch.insert("Receipt", map,
          conflictAlgorithm: ConflictAlgorithm.replace);
    }

    await batch.commit();

    return true;
  }

  Future<bool> deleteTable() async {
    final db = await _localStorageSQLite.database;
    await db.delete("Receipt");
    return true;
  }
}
