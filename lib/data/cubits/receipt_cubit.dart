import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/models/receipt.dart';
import 'package:lodge/data/repositories/receipt_repository.dart';
import 'package:lodge/data/state/receipts_state.dart';
import 'package:lodge/data/state/connectivity_state.dart';

import 'connectivity_cubit.dart';

class ReceiptCubit extends Cubit<ReceiptState> {
  ReceiptCubit({required this.connectionCubit}) : super(InitialReceiptState()) {
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if (connectivity is Connected) {
        isConnected = true;
      } else {
        isConnected = false;
      }
    });
    getReceipts();
  }

  final ConnectivityCubit connectionCubit;
  late final StreamSubscription connectionStreamSub;
  late bool isConnected;

  final repository = ReceiptRepository();

  void getReceipts() async {
    try {
      if (state is InitialReceiptState) {
        emit(LoadingReceiptState());
        final receipts = isConnected
            ? await repository.getAllFirestoreStoreLocal(20)
            : await repository.getAllLocalStorage(0, 20);
        emit(LoadedReceiptState(receipts, false));
      } else if (state is LoadedReceiptState &&
          (state as LoadedReceiptState).receipts.isNotEmpty) {
        int length = (state as LoadedReceiptState).receipts.length;
        List<ReceiptModel> receipts = isConnected
            ? await repository.getAllFirestore(
                (state as LoadedReceiptState).receipts[length - 1].id!, 20)
            : await repository.getAllLocalStorage(length, 20);
        if (receipts.isEmpty) {
          emit((state as LoadedReceiptState).copyWith(hasReachedMax: true));
        } else {
          emit(LoadedReceiptState(
              (state as LoadedReceiptState).receipts + receipts, false));
        }
      }
    } catch (e, s) {
      print(s);
      emit(ErrorReceiptState(e));
    }
  }
}
