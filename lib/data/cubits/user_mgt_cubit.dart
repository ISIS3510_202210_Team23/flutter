import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/dao/local_storage/local_storage_complex.dart';
import 'package:lodge/data/models/user.dart';
import 'package:lodge/data/repositories/complex_repository.dart';
import 'package:lodge/data/state/connectivity_state.dart';

class UserMgtCubit extends Cubit<UserState>{

  UserMgtCubit({required this.connectionCubit}): super(const UserState()){
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if(connectivity is Connected){
        isConnected = true;
      }
      else{
        isConnected = false;
      }
    });
  }

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _storage = FirebaseFirestore.instance;
  final String status = "";

  final repository = ComplexRepository();
  final localStorageComplexInfo = LocalStorageComplex();
  final ConnectivityCubit connectionCubit;
  StreamSubscription? connectionStreamSub;

  late bool isConnected;

  User? getUser(){
    User? user = _auth.currentUser;
    if(user != null){
      emit(state.copyWith(user: user));
      return user;
    }
    return null;
  }

  void listenUser(){
    _auth.authStateChanges().listen((User? user) {
      if (user == null) {
        emit(state.copyWith(user: null));
      } else {
        emit(state.copyWith(user: user, email: user.email, id: user.uid));
      }
     });
  }

  void setUser(User? user) async {
    if (user == null) {
      emit(state.copyWith(user: null));
    } 
    else {
      emit(state.copyWith(user: user));
      // if(!isConnected){
      //   try{
      //     final localData = await localStorageComplexInfo.get(null);
      //     final localExtraData = await localStorageComplexInfo.getExtraInfo();
      //     emit(state.copyWith(
      //         user: user,
      //         email: user.email,
      //         id: user.uid,
      //         complex: localData,
      //         house: localExtraData?["house"],
      //         name: localExtraData?["name"]
      //       ));
      //     return;
      //   }
      //   catch(e){
      //     emit(state.copyWith(
      //         user: user,
      //         email: user.email,
      //         id: user.uid,
      //       ));
      //   }
      // }
      
      // DocumentSnapshot document =
      //     await _storage.collection('Users').doc(user.uid).get();
      // Map<String, dynamic> data = document.data() as Map<String, dynamic>;

      // localStorageComplexInfo.postExtraInfo({"name": data["name"], "house": data["house"]});
      
      // try {
      //   final complexData = await repository.get(data["complex"]);
      //   emit(state.copyWith(
      //       user: user,
      //       email: user.email,
      //       id: user.uid,
      //       complex: complexData,
      //       house: data["house"],
      //       name: data["name"]));
      // } catch (e) {
      //   emit(state.copyWith(
      //       user: user,
      //       email: user.email,
      //       id: user.uid,
      //       complex: null,
      //       house: data["house"],
      //       name: data["name"]));
      // }
    }
  }

  void signOut(){
    _auth.signOut();
    emit((state.copyWith(user: null)));
  }

  String _getUserId(){
    return state.id;
  }

}