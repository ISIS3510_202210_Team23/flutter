import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/models/coneccion.dart';

import 'package:lodge/data/state/connectivity_state.dart';

class ChatBotCubit extends Cubit<ConexionStatus> {
  bool isConnected = true;

  ChatBotCubit({required this.connectionCubit}) : super(const ConexionStatus()) {

    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if (connectivity is Connected) {
        isConnected = true;
      } else {

        isConnected = false;
      }

    });
    onSubmit();
  }

  final ConnectivityCubit connectionCubit;
  StreamSubscription? connectionStreamSub;

  void noInternetError() {
    emit(state.copyWith(status: "Connection"));
  }
  String noInternetErrorE() {
    if (!isConnected) {
      noInternetError();
      return "Connection";
    }
    return "";
  }
  void resetStateStatus() {
    emit(state.copyWith(status: ""));
  }

  void onSubmit() async {
    if (!isConnected) {
      noInternetError();
      return;
    }
  }
}
