import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/models/login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:lodge/data/state/connectivity_state.dart';

class LoginCubit extends Cubit<LoginState>{
  LoginCubit({required this.connectionCubit}): super(const LoginState()){
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if(connectivity is Connected){
        isConnected = true;
      }
      else{
        isConnected = false;
      }
    });
  }

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final ConnectivityCubit connectionCubit;
  StreamSubscription? connectionStreamSub;

  bool isConnected = true;

  void isLoggedIn(){
    if(_auth.currentUser == null){
      emit(state.copyWith(user: _auth.currentUser));
    }
    else{
      emit(state.copyWith(user: null));
    }
  }

  void onChangeEmail(String value){
    emit(state.copyWith(email: value));
  }

  void onPasswordChange(String value){
    emit(state.copyWith(password: value));
  }

  void onSubmit() async{
    if(!isConnected){
      noInternetError();
      return;
    }
    try{
      emit(state.copyWith(status: "Submitting"));
      UserCredential userCredential = await _auth.signInWithEmailAndPassword(email: state.email, password: state.password);
      emit(state.copyWith(status: "LoggedIn", user: userCredential.user));
    }
    on FirebaseAuthException catch(error){
      emit(state.copyWith(status: "Error", message: "wrong user or pasword"));
    }
    catch(error){
      emit(state.copyWith(status: "Error", message: "An unexpected error has ocurred, try again later"));
    }
  }

  void googleLogin() async{
    try{
      emit(state.copyWith(status: "Submitting"));
      final GoogleSignInAccount? googleUser = await GoogleSignIn().signIn();

      final GoogleSignInAuthentication? googleAuth = await googleUser?.authentication;

      final credential = GoogleAuthProvider.credential(
        accessToken: googleAuth?.accessToken,
        idToken: googleAuth?.idToken,
      );

      UserCredential userCredential = await _auth.signInWithCredential(credential);
      emit(state.copyWith(status: "LoggedIn", user: userCredential.user));
    }
    on FirebaseAuthException catch(error){
      emit(state.copyWith(status: "Error"));
    }
    catch(error){
      emit(state.copyWith(status: "Error"));
    }
  }

  void noInternetError(){
    emit(state.copyWith(status: "Connection"));
  }

  void resetStateStatus(){
    emit(state.copyWith(status: "", message: null));
  }

  String? _validate(String? value, String field){
    if(field == "email"){
      if(value == null || value.isEmpty || value.trim() == "") {
        return "Email required";
      }
      else if(RegExp(r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$').hasMatch(value)){
        return null;
      }
      else{
        return "Please use a valid email";
      }
    }
    else{
      if(value == null || value.isEmpty || value.trim() == ""){
        return "Pasword required";
      }
      else{
        return null;
      }
    }
  }
}