import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/dao/local_storage/local_storage_profile.dart';
import 'package:lodge/data/models/residentModel.dart';
import 'package:lodge/data/repositories/user_repository.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/data/state/profile_state.dart';

class ProfileCubit extends Cubit<ProfileState>{
  ProfileCubit({required this.connectionCubit}) : super(InitialProfileState()){
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if (connectivity is Connected) {
        isConnected = true;
        getUserData();
      } else {
        isConnected = false;
      }
    });
    getUserData();
  }

  final ConnectivityCubit connectionCubit;
  late final StreamSubscription connectionStreamSub;
  late bool isConnected;

  final UserRepository _repository = UserRepository();
  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final LocalStorageProfile _localStorageProfile = LocalStorageProfile();

  void getUserData() async{
    emit(LoadingProfileState());
    if(!isConnected){
      try{
        final localInfo = await _localStorageProfile.getProfileInfo();
        emit(LoadedProfileState(localInfo));
        return;
      }
      catch(e){
        emit(ErrorProfileState("Error retreiving info, please check your internet connection"));
        return;
      }
    }
    if(_firebaseAuth.currentUser == null){
      emit(ErrorProfileState("Please log in again to see your profile"));
      return;
    }
    else{
      try{
        ResidentModel user = await _repository.getUser(_firebaseAuth.currentUser?.uid);
        emit(LoadedProfileState(user));
      }
      catch(e){
        try{
          final localInfo = await _localStorageProfile.getProfileInfo();
          emit(LoadedProfileState(localInfo));
        }
        catch(e){
          emit(ErrorProfileState("Error retreiving info, please check your internet connection"));
        }
      }
    }
  }
  
}