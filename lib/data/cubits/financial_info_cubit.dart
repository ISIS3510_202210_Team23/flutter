import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/repositories/financial_info_repository.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/data/state/financial_info_state.dart';

class FinancialInfoCubit extends Cubit<FinancialInfoState> {
  FinancialInfoCubit({required this.connectionCubit})
      : super(InitialFinancialInfoState()) {
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if (connectivity is Connected) {
        isConnected = true;
      } else {
        isConnected = false;
      }
    });
    getFinancialInfo();
  }

  final ConnectivityCubit connectionCubit;
  late final StreamSubscription connectionStreamSub;
  late bool isConnected;

  final repository = FinancialInfoRepository();

  void getFinancialInfo() async {
    try {
      emit(LoadingFinancialInfoState());
      var financialInfo = isConnected
          ? await repository.getFirestoreFinancialInfo()
          : await repository.getLocalFinancialInfo();
      emit(FinancialInfoLoadedState(financialInfo));
    } catch (e, s) {
      print(s);
      emit(ErrorFinancialInfoState(e));
    }
  }
}
