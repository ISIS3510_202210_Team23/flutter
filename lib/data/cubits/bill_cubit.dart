import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/repositories/bill_repository.dart';
import 'package:lodge/data/state/bills_state.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:firebase_performance/firebase_performance.dart';

import 'connectivity_cubit.dart';

class BillCubit extends Cubit<BillsState> {
  BillCubit({required this.connectionCubit}) : super(InitialBillsState()) {
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if (connectivity is Connected) {
        isConnected = true;
      } else {
        isConnected = false;
      }
    });
    getBills();
  }

  FirebasePerformance performance = FirebasePerformance.instance;

  final ConnectivityCubit connectionCubit;
  late final StreamSubscription connectionStreamSub;
  late bool isConnected;

  final repository = BillRepository();

  void getBills() async {
    try {
      if (state is InitialBillsState) {
        emit(LoadingBillsState());
        var bills = isConnected
            ? await repository.getAllFirestoreStoreLocal(20)
            : await repository.getAllLocalStorage(0, 20);
        emit(LoadedBillsState(bills, false));
      } else if (state is LoadedBillsState &&
          (state as LoadedBillsState).bills.isNotEmpty) {
        var billTrace = performance.newTrace("BillTrace");
        await billTrace.start();
        int length = (state as LoadedBillsState).bills.length;
        List<BillModel> bills = isConnected
            ? await repository.getAllFirestore(
                (state as LoadedBillsState).bills[length - 1].id!, 20)
            : await repository.getAllLocalStorage(length, 20);
        if (bills.isEmpty) {
          emit((state as LoadedBillsState).copyWith(hasReachedMax: true));
        } else {
          emit(LoadedBillsState(
              (state as LoadedBillsState).bills + bills, false));
        }
        await billTrace.stop();
      }
    } catch (e, s) {
      print(s);
      emit(ErrorBillsState(e));
    }
  }
}
