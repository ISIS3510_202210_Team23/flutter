import 'dart:async';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/models/complexModel.dart';
import 'package:lodge/data/models/register.dart';
import 'package:lodge/data/models/residentModel.dart';
import 'package:lodge/data/repositories/complex_repository.dart';
import 'package:lodge/data/state/connectivity_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit({required this.connectionCubit}) : super(const RegisterState()){
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if(connectivity is Connected){
        isConnected = true;
      }
      else{
        isConnected = false;
      }
    });
  }

  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseFirestore _storage = FirebaseFirestore.instance;
  final repository = ComplexRepository();

  final ConnectivityCubit connectionCubit;
  StreamSubscription? connectionStreamSub;

  bool isConnected = true;

  void setFields(field, value) {
    switch (field) {
      case "name":
        emit(state.copyWith(name: value));
        break;
      case "username":
        emit(state.copyWith(username: value));
        break;
      case "email":
        emit(state.copyWith(email: value));
        break;
      case "phone":
        emit(state.copyWith(phone: value));
        break;
      case "gender":
        emit(state.copyWith(gender: value));
        break;
      case "admin":
        emit(state.copyWith(admin: value));
        break;
      case "password":
        emit(state.copyWith(password: value));
        break;
      case "passwordConfirm":
        emit(state.copyWith(passwordConfirm: value));
        break;
      case "code":
        emit(state.copyWith(code: value));
        break;
      case "status":
        emit(state.copyWith(status: value));
        break;
      case "complexName":
        emit(state.copyWith(complexName: value));
        break;
      case "city":
        emit(state.copyWith(city: value));
        break;
      case "address":
        emit(state.copyWith(address: value));
        break;
    }
  }

  void setCreateComplexFields() {
    emit(state.copyWith(status: "Loading"));
    registerComplex([state.complexName, state.city, state.address]);
  }

  void resetState() {
    emit(const RegisterState());
  }

  Future<UserCredential> createUserWithCred(String email, String password) async{
    FirebaseApp app = await Firebase.initializeApp(name: "Secondary", options: Firebase.app().options);
    try{
      UserCredential userCredential = await FirebaseAuth.instanceFor(app: app)
      .createUserWithEmailAndPassword(email: email, password: password);
      await app.delete();
      return userCredential;
    }on FirebaseAuthException {
      await app.delete();
      rethrow;
    }
  }

  void registerComplex(values) async {
    if(!isConnected){
      connectivityError();
      return;
    }
    try {
      UserCredential result = await createUserWithCred(state.email,state.password);
      User? user = result.user;
        if(user != null){
            // await _auth.signOut();
            await _storage.collection('Users').doc(user.uid).set({
              "name": state.name,
              "username": state.username,
              "email": state.email,
              "gender": state.gender?"Female":"Male",
              "phone": state.phone,
              "house": state.code,
              "isAdmin": state.admin,
              "userImage": ""
            });

        repository.post(
            ComplexModel(name: values[0], city: values[1], address: values[2], admin: ResidentModel(house: state.code, name: state.name, phone: state.phone, id: user.uid, admin: state.admin, email: state.email, complex: "", image: "")));
        emit(state.copyWith(status: "Success"));
      }
    } on FirebaseAuthException catch (e) {
      if (e.code == 'email-already-in-use') {
        emit(state.copyWith(status: "Email already in use"));
      } else if (e.code == 'operation-not-allowed') {
        emit(state.copyWith(
            status: "A problem has ocurred, please try again later"));
      } else if (e.code == 'invalid-email') {
        emit(state.copyWith(status: "Invalid email address"));
      } else {
        emit(state.copyWith(status: "code-error"));
      }
    } catch (e) {
      // print(e);
      emit(state.copyWith(status: "code-error"));
    }
  }

  void connectivityError(){
    emit(state.copyWith(status: "Connection"));
  }

  void registerUser() async {
    if(!isConnected){
      connectivityError();
      return;
    }
    emit(state.copyWith(status: "Loading"));
    String? codeValid = await isCodeValid();
    if (codeValid != null) {
      try {
        UserCredential result = await createUserWithCred(state.email, state.password);
        User? user = result.user;
        if (user != null) {
          // await _auth.signOut();
          await _storage.collection('Users').doc(user.uid).set({
            "name": state.name,
            "username": state.username,
            "email": state.email,
            "gender": state.gender ? "Female":"Male",
            "phone": state.phone,
            "house": state.code,
            "isAdmin": state.admin,
            "complex": codeValid,
            "userImage": ""
          });
          emit(state.copyWith(status: "Success"));
        }
      } on FirebaseAuthException catch (e) {
        if (e.code == 'email-already-in-use') {
          emit(state.copyWith(status: "Email already in use"));
        } else if (e.code == 'operation-not-allowed') {
          emit(state.copyWith(
              status: "A problem has ocurred, please try again later"));
        } else if (e.code == 'invalid-email') {
          emit(state.copyWith(status: "Invalid email address"));
        } else {
          emit(state.copyWith(status: "code-error"));
        }
      } catch (e) {
        emit(state.copyWith(status: "code-error"));
      }
    } else {
      emit(state.copyWith(status: "Complex code error, make sure your code is correct"));
    }
  }

  Future<String?> isCodeValid() async {
    try {
      DocumentSnapshot<Map<String, dynamic>> document =
          await _storage.collection('Houses').doc(state.code).get();
      if (document.exists) {
        final houseData = document.data();
        if (houseData != null) {
          return houseData["complex"];
        } else {
          return null;
        }
      } else {
        return null;
      }
    } catch (e) {
      return null;
    }
  }

  String? validation(field, String? value) {
    if (field != "gender" && field != "admin") {
      if (value == null || value.isEmpty || value.trim() == "") {
        return field == "passwordConfirm" ? "Required" : field + " required";
      }
    }

    switch (field) {
      case "name":
        if (!RegExp(r'[^\d_!¡?÷?¿/\\+=@#$%ˆ&*(){}|~<>;:[\]]')
            .hasMatch(value!)) {
          return "Invalid name";
        }
        break;
      case "email":
        if (!RegExp(
                r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
            .hasMatch(value!)) {
          return "Invalid email";
        }
        break;
      case "phone":
        if (!RegExp(r'^[1-9]{1}\d{9}$').hasMatch(value!)) {
          if (!RegExp(r'^[1-9]\d{6}$').hasMatch(value)) {
            return "Invalid phone number";
          }
        }
        break;
      case "password":
        if (value != null) {
          if (value.length < 8) {
            return "Password must be 8 characters long";
          }
        }
        if (!RegExp(
                r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
            .hasMatch(value!)) {
          return "Password must have:\n1 Upper case Character \n1 Lower case Character \n1 numeric character \n1 Special character";
        }
        break;
      case "passwordConfirm":
        if (value != state.password) {
          return "Password must be the same";
        }
        break;
    }
    return null;
  }
}
