import 'dart:async';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/models/tab_stats_model.dart';
import 'package:lodge/data/repositories/tab_state_repository.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/data/state/tabs_state.dart';

class TabCubit extends Cubit<TabState>{
  TabCubit({required this.connectionCubit}) : super(HomeTab()){
    {
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if(connectivity is Connected){
        isConnected = true;
      }
      else{
        isConnected = false;
      }
    });
  }
  }

  final TabRepository _tabRepository = TabRepository();
  final connectionCubit;
  StreamSubscription? connectionStreamSub;
  late bool isConnected;
  
  void setTabStats(String name, String before){
    if(!isConnected){
      return;
    }
    final time = DateTime.now().millisecondsSinceEpoch - state.initialTime;
    _tabRepository.updateStats(TabStatModel(before, time));

    TabState tabState = HomeTab();
    switch (name) {
      case "Home":
        tabState = HomeTab();
        break;
      case "Profile":
        tabState = ProfileTab();
        break;
      case "Residential Complex":
        tabState = ResidentialComplexTab();
        break;
      case "Payments":
        tabState = PaymentsTab();
        break;
      case "Smart Assistant":
        tabState = AssistantTab();
        break;
      default:
    }
    emit(tabState);
  }
}