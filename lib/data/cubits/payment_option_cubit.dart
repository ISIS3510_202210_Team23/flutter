import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/models/payment_option.dart';
import 'package:lodge/data/dao/dao_interface.dart';
import 'package:lodge/data/state/payment_option_state.dart';

class PaymentOptionCubit extends Cubit<PaymentOptionState> {
  PaymentOptionCubit({required this.repository})
      : super(InitialPaymentOptionState()) {
    getPaymentOptions();
  }

  final DaoInterface<PaymentOptionModel> repository;

  void getPaymentOptions() async {
    try {
      emit(LoadingPaymentOptionState());
      final paymentOptions = await repository.getAll(null, null);
      emit(LoadedPaymentOptionState(paymentOptions));
    } catch (e) {
      emit(ErrorPaymentOptionState(e));
    }
  }
}
