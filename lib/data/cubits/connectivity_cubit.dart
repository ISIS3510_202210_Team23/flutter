import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/state/connectivity_state.dart';



class ConnectivityCubit extends Cubit<ConnectivityState>{
  ConnectivityCubit({required ConnectivityResult initial}) : super(UnknownConnectivity()){
    checkConnectivity(initial);
    listenToConnectivity();
  }

  void listenToConnectivity(){
    final conn = Connectivity();
    conn.onConnectivityChanged.listen((ConnectivityResult result) {
      if(result == ConnectivityResult.ethernet || result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){
        emit(Connected());
      }
      else{
        emit(Disconnected());
      }
    });
  }

  void checkConnectivity(ConnectivityResult result) {
    if(result == ConnectivityResult.ethernet || result == ConnectivityResult.mobile || result == ConnectivityResult.wifi){
        emit(Connected());
      }
      else{
        emit(Disconnected());
      }
  }
}