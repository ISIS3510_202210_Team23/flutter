import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/models/complexModel.dart';
import 'package:lodge/data/models/residentModel.dart';
import 'package:lodge/data/repositories/complex_repository.dart';
import 'package:lodge/data/repositories/user_repository.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/data/state/residential_complex_state.dart';

class ResComplexCubit extends Cubit<ResidentialComplexState> {
  ResComplexCubit({required this.connectionCubit})
      : super(InitialResComplexState()) {
    isConnected = connectionCubit.state is Connected;
    connectionStreamSub = connectionCubit.stream.listen((connectivity) {
      if (connectivity is Connected) {
        isConnected = true;
        getResidentialComplexInfo();
      } else {
        isConnected = false;
      }
    });
    getResidentialComplexInfo();
  }

  final FirebaseAuth _firebaseAuth = FirebaseAuth.instance;
  final ComplexRepository _complexRepository = ComplexRepository();
  final UserRepository _userRepository = UserRepository();

  final ConnectivityCubit connectionCubit;
  late final StreamSubscription connectionStreamSub;
  late bool isConnected;

  void getResidentialComplexInfo() async {
    emit(LoadingResComplexState());
    if (isConnected) {
      try {
        String? id = _firebaseAuth.currentUser?.uid;
        if (id == null) {
          emit(ErrorResComplexState());
          return;
        }
        ResidentModel user = await _userRepository.getUser(id);
        ComplexModel complex = await _complexRepository.get(user.complex);
        emit(LoadedResComplexState(complex));
      } catch (e) {
        emit(ErrorResComplexState());
      }
    } else {
      try {
        ComplexModel complexLocal =
            await _complexRepository.getLocalResidentialComplexData();
        emit(LoadedResComplexState(complexLocal));
      } catch (e) {
        emit(ErrorResComplexState());
      }
    }
  }

  void postBanner(String path) async {
    if (isConnected && state is LoadedResComplexState) {
      try {
        String image = await _complexRepository.postBanner(
            (state as LoadedResComplexState).complexModel, path);
        emit(LoadedResComplexState((state as LoadedResComplexState)
            .complexModel
            .copyWith(image: image)));
      } catch (e) {
        emit(ErrorResComplexState());
      }
    }
  }
}
