import 'package:equatable/equatable.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/models/payment_option.dart';

abstract class PaymentOptionState extends Equatable {}

class InitialPaymentOptionState extends PaymentOptionState {
  @override
  List<Object?> get props => [];
}

class LoadingPaymentOptionState extends PaymentOptionState {
  @override
  List<Object?> get props => [];
}

class LoadedPaymentOptionState extends PaymentOptionState {
  final List<PaymentOptionModel> paymentOptions;

  LoadedPaymentOptionState(this.paymentOptions);

  @override
  List<Object?> get props => [paymentOptions];
}

class ErrorPaymentOptionState extends PaymentOptionState {
  final Object error;

  ErrorPaymentOptionState(this.error);

  @override
  List<Object?> get props => [error];
}
