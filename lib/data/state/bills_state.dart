import 'package:equatable/equatable.dart';
import 'package:lodge/data/models/bill.dart';

abstract class BillsState extends Equatable {}

class InitialBillsState extends BillsState {
  @override
  List<Object?> get props => [];
}

class LoadingBillsState extends BillsState {
  @override
  List<Object?> get props => [];
}

class LoadedBillsState extends BillsState {
  final List<BillModel> bills;
  final bool hasReachedMax;

  LoadedBillsState(this.bills, this.hasReachedMax);

  @override
  List<Object?> get props => [bills, hasReachedMax];

  LoadedBillsState copyWith({
    List<BillModel>? bills,
    bool? hasReachedMax,
  }) {
    return LoadedBillsState(
      bills ?? this.bills,
      hasReachedMax ?? this.hasReachedMax,
    );
  }
}

class ErrorBillsState extends BillsState {
  final Object error;

  ErrorBillsState(this.error);

  @override
  List<Object?> get props => [error];
}
