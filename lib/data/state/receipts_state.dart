import 'package:equatable/equatable.dart';
import 'package:lodge/data/models/receipt.dart';

abstract class ReceiptState extends Equatable {}

class InitialReceiptState extends ReceiptState {
  @override
  List<Object?> get props => [];
}

class LoadingReceiptState extends ReceiptState {
  @override
  List<Object?> get props => [];
}

class LoadedReceiptState extends ReceiptState {
  final List<ReceiptModel> receipts;
  final bool hasReachedMax;

  LoadedReceiptState(this.receipts, this.hasReachedMax);

  @override
  List<Object?> get props => [receipts, hasReachedMax];

  LoadedReceiptState copyWith({
    List<ReceiptModel>? receipts,
    bool? hasReachedMax,
  }) {
    return LoadedReceiptState(
      receipts ?? this.receipts,
      hasReachedMax ?? this.hasReachedMax,
    );
  }
}

class ErrorReceiptState extends ReceiptState {
  final Object error;

  ErrorReceiptState(this.error);

  @override
  List<Object?> get props => [error];
}
