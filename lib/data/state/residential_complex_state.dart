import 'package:equatable/equatable.dart';
import 'package:lodge/data/models/complexModel.dart';

abstract class ResidentialComplexState extends Equatable{
}

class InitialResComplexState extends ResidentialComplexState{
  @override
  List<Object?> get props => [];
}

class LoadingResComplexState extends ResidentialComplexState{
  @override
  List<Object?> get props => [];
}

class LoadedResComplexState extends ResidentialComplexState{
  final ComplexModel complexModel;

  LoadedResComplexState(this.complexModel);

  @override
  List<Object?> get props => [complexModel];
}

class ErrorResComplexState extends ResidentialComplexState{
  @override
  List<Object?> get props => [];
}