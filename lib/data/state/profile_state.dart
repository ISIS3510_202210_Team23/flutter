import 'package:equatable/equatable.dart';
import 'package:lodge/data/models/residentModel.dart';

abstract class ProfileState extends Equatable{}

class InitialProfileState extends ProfileState{
  @override
  List<Object?> get props => [];
}

class LoadingProfileState extends ProfileState{
  @override
  List<Object?> get props => [];
}

class LoadedProfileState extends ProfileState{
  final ResidentModel model;

  LoadedProfileState(this.model);

  @override
  List<Object?> get props => [model];
}

class ErrorProfileState extends ProfileState{
  final String message;

  ErrorProfileState(this.message);

  @override
  List<Object?> get props => [message];
}
