import 'package:equatable/equatable.dart';
import 'package:lodge/data/models/financial_info_model.dart';

abstract class FinancialInfoState extends Equatable {}

class InitialFinancialInfoState extends FinancialInfoState {
  @override
  List<Object?> get props => [];
}

class LoadingFinancialInfoState extends FinancialInfoState {
  @override
  List<Object?> get props => [];
}

class FinancialInfoLoadedState extends FinancialInfoState {
  final FinancialInfoModel financialInfo;

  FinancialInfoLoadedState(this.financialInfo);

  @override
  List<Object?> get props => [financialInfo];
}

class ErrorFinancialInfoState extends FinancialInfoState {
  final Object error;

  ErrorFinancialInfoState(this.error);

  @override
  List<Object?> get props => [error];
}
