import 'package:equatable/equatable.dart';

abstract class ConnectivityState extends Equatable{}

class UnknownConnectivity extends ConnectivityState{
  @override
  List<Object?> get props => [];
  
}

class Connected extends ConnectivityState{
  @override
  List<Object?> get props => [];
  
}

class Disconnected extends ConnectivityState{
  @override
  List<Object?> get props => [];
}