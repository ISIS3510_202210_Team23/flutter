import 'package:equatable/equatable.dart';

abstract class TabState extends Equatable{
  final initialTime = DateTime.now().millisecondsSinceEpoch;
}

class HomeTab extends TabState{

  @override
  List<Object?> get props => [];
}

class ProfileTab extends TabState{

  @override
  List<Object?> get props => [];
}

class ResidentialComplexTab extends TabState{

  @override
  List<Object?> get props => [];
}

class PaymentsTab extends TabState{

  @override
  List<Object?> get props => [];
}

class AssistantTab extends TabState{

  @override
  List<Object?> get props => [];
}