import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/models/model_interface.dart';
import 'package:lodge/data/models/payment_option.dart';

class ReceiptModel implements ModelInterface {
  final String? id;
  final BillModel bill;
  final Timestamp date;
  final PaymentOptionModel? paymentOption;

  const ReceiptModel(
      {this.id, required this.bill, required this.date, this.paymentOption});

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = {
      "date": date,
      "bill": bill.toMap(),
    };

    if (id != null) map["id"] = id;
    if (paymentOption != null) map["paymentOption"] = paymentOption?.toMap();

    return map;
  }

  static ReceiptModel fromMap(Map<String, dynamic> map) {
    return ReceiptModel(
      id: map["id"] as String,
      bill: BillModel.fromMap(map["bill"] as Map<String, dynamic>),
      date: map["date"] as Timestamp,
      paymentOption: PaymentOptionModel.fromMap(
          map["paymentOption"] as Map<String, dynamic>),
    );
  }


}
