import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:lodge/data/models/complexModel.dart';

class UserState extends Equatable{

  const UserState({
    this.name = "",
    this.email = "",
    this.id = "",
    this.user,
    this.complex,
    this.house = ""
  });

  final String name;
  final String email;
  final String id;
  final User? user;

  final ComplexModel? complex;
  final String house;

  @override

  List<Object?> get props => [name, email, id, user, complex, house];

  UserState copyWith({
    String? name,
    String? email,
    String? id,
    User? user,
    ComplexModel? complex,
    String? house,
  }){
    return UserState(
      name: name?? this.name,
      email: email?? this.email,
      id: id?? this.id,
      user: user?? this.user,
      complex: complex?? this.complex,
      house: house?? this.house,
    );
  }
  
}