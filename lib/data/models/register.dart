import 'package:equatable/equatable.dart';
import 'package:lodge/data/models/complexModel.dart';

class RegisterState extends Equatable {
  const RegisterState({
    this.name = "",
    this.username = "",
    this.email = "",
    this.phone = "",
    this.gender = false,
    this.admin = false,
    this.password = "",
    this.passwordConfirm = "",
    this.code = "",
    this.status = "",
    this.complexInfo,
    this.complexName = "",
    this.city = "",
    this.address = ""
  });

  final ComplexModel? complexInfo;
  final String name;
  final String username;
  final String email;
  final String phone;
  final bool gender;
  final bool admin;
  final String password;
  final String passwordConfirm;
  final String code;
  final String status;
  final String complexName;
  final String city;
  final String address;

  @override
  List<Object?> get props => [name, username, email, phone, gender, admin, password, passwordConfirm, code, status, complexInfo, complexName, city, address];

  RegisterState copyWith({
    String? name,
    String? username,
    String? email,
    String? phone,
    bool? gender,
    bool? admin,
    String? password,
    String? passwordConfirm,
    List? errors,
    bool? doneFirst,
    String? code,
    String? status,
    ComplexModel? complexInfo,
    String? complexName,
    String? city,
    String? address,
    }
    ){
      return RegisterState(
       name: name?? this.name,
       username: username?? this.username,
       email: email?? this.email,
       phone: phone?? this.phone,
       gender: gender?? this.gender,
       admin: admin?? this.admin,
       password: password?? this.password,
       passwordConfirm: passwordConfirm?? this.passwordConfirm,
       code: code?? this.code,
       status: status?? this.status,
       complexInfo: complexInfo?? this.complexInfo,
       complexName: complexName?? this.complexName,
       city: city?? this.city,
       address: address?? this.address,
      );
    }
}