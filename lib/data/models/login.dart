import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';

class LoginState extends Equatable {
  const LoginState({
    this.email = "", this.password = "", this.status = "", this.loggedIn = false, this.user, this.message
  });

  final String email;
  final String password;
  final String status;
  final bool loggedIn;
  final User? user;
  final String? message;

  @override
  List<Object?> get props => [email, password, status];

  LoginState copyWith({
    String? email,
    String? password,
    String? status,
    bool? loggedIn,
    User? user,
    String? message}
    ){
      return LoginState(
       email: email?? this.email,
       password: password?? this.password, 
       status: status?? this.status, 
       loggedIn: loggedIn?? this.loggedIn,
       user: user?? this.user,
       message: message??this.message);
    }
}