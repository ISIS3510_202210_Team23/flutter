class FinancialInfoModel {
  final int totalBills;
  final int billsPaid;
  late final double paidPercentage;
  late final double unpaidPercentage;

  FinancialInfoModel({required this.totalBills, required this.billsPaid}) {
    paidPercentage = totalBills != 0 ? (billsPaid / totalBills) * 100 : 0;
    unpaidPercentage = 100 - paidPercentage;
  }

  String get paidPercentageString => '${paidPercentage.toStringAsFixed(0)}%';

  String get unpaidPercentageString =>
      '${unpaidPercentage.toStringAsFixed(0)}%';
}
