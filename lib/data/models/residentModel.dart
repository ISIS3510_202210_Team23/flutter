import 'package:lodge/data/models/model_interface.dart';

class ResidentModel extends ModelInterface {

  ResidentModel({
    required this.id,
    required this.name,
    required this.phone,
    required this.house,
    required this.email,
    required this.admin,
    required this.complex,
    required this.image
  });

  final String email;
  final String id;
  final String name;
  final String phone;
  final String house;
  final bool admin;
  final String complex;
  final String image;

  @override
  Map<String, dynamic> toMap() {
    return {
      "id": id,
      "name": name,
      "phone": phone,
      "house": house,
      "email": email,
      "admin": admin,
      "complex": complex,
      "image": image
    };
  }

  ResidentModel.fromMap(Map<String, dynamic> map)
    : id = map["id"],
      name = map["name"],
      phone = map["phone"],
      house = map["house"],
      email = map["email"],
      admin = map["admin"],
      complex = map["complex"],
      image = map["image"];
  
}