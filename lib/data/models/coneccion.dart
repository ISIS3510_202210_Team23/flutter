import 'package:equatable/equatable.dart';

class ConexionStatus extends Equatable {
  const ConexionStatus({this.status = ""});

  final String status;

  @override
  List<Object?> get props => [status];

  ConexionStatus copyWith({String? status}) {
    return ConexionStatus(
      status: status ?? this.status,
    );
  }
}
