import 'package:flutter/material.dart';
import 'package:lodge/data/models/model_interface.dart';

class PaymentOptionModel implements ModelInterface {
  final String name;
  final String description;
  late final IconData icon;

  PaymentOptionModel(
      {required this.name, required this.description, required int icon}) {
    this.icon = IconData(icon, fontFamily: 'MaterialIcons');
  }

  @override
  Map<String, dynamic> toMap() {
    return {
      "name": name,
      "description": description,
      "icon": icon.codePoint
    };
  }

  static PaymentOptionModel fromMap(Map<String, dynamic> map) {
    return PaymentOptionModel(
        name: map["name"], description: map["description"], icon: map["icon"]);
  }
}
