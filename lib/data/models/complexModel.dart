import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:lodge/data/models/model_interface.dart';
import 'package:lodge/data/models/residentModel.dart';

class ComplexModel implements ModelInterface {
  const ComplexModel({
    required this.name,
    required this.city,
    required this.address,
    this.image,
    this.admin,
    this.geoLocation,
    this.id,
  });

  final String name;
  final String city;
  final String address;
  final String? image;
  final ResidentModel? admin;
  final GeoPoint? geoLocation;
  final String? id;

  @override
  Map<String, dynamic> toMap() {
    Map<String, String?>? adminMap;
    if (admin != null) {
      adminMap = {
        "name": admin?.name,
        "phone": admin?.phone,
        "house": admin?.house,
        "id": admin?.id,
        "email": admin?.email
      };
    }
    return {
      "name": name,
      "city": city,
      "address": address,
      "image": image,
      "admin": adminMap,
      "geoLocation": [geoLocation?.latitude, geoLocation?.longitude]
    };
  }

  ComplexModel.fromMap(Map<String, dynamic> map)
      : id = map["id"],
        name = map['name'],
        city = map['city'],
        address = map['address'],
        image = map['image'],
        admin = map['admin'] != null
            ? ResidentModel(
                name: map['admin']['name'],
                phone: map['admin']['phone'],
                house: map['admin']['house'],
                id: map['admin']['id'],
                admin: true,
                email: map['admin']['email'],
                complex: "",
                image: "")
            : null,
        geoLocation = GeoPoint(map['geoLocation'][0], map['geoLocation'][1]);

  ComplexModel copyWith({
    String? name,
    String? city,
    String? address,
    String? image,
    ResidentModel? admin,
    GeoPoint? geoLocation,
    String? id,
  }) {
    return ComplexModel(
      name: name ?? this.name,
      city: city ?? this.city,
      address: address ?? this.address,
      image: image ?? this.image,
      admin: admin ?? this.admin,
      geoLocation: geoLocation ?? this.geoLocation,
      id: id ?? this.id,
    );
  }
}
