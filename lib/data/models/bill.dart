import 'package:cloud_firestore/cloud_firestore.dart';

import 'model_interface.dart';

class BillModel implements ModelInterface {
  final String? id;
  final String name;
  final double amount;
  final Timestamp dueDate;
  final Timestamp publishDate;
  final bool paid;

  const BillModel({
    this.id,
    required this.name,
    required this.amount,
    required this.dueDate,
    required this.publishDate,
    required this.paid,
  });

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = {
      "name": name,
      "amount": amount,
      "dueDate": dueDate,
      "publishDate": publishDate,
      "paid": paid
    };
    if (id != null) map["id"] = id;

    return map;
  }

  static BillModel fromMap(Map<String, dynamic> map) {
    return BillModel(
      id: map["id"] as String,
      name: map["name"] as String,
      amount: map["amount"] as double,
      dueDate: map["dueDate"] as Timestamp,
      publishDate: map["publishDate"] as Timestamp,
      paid: map["paid"] as bool,
    );
  }
}
