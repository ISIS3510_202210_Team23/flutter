import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/payment_option_cubit.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/models/payment_option.dart';
import 'package:lodge/data/models/receipt.dart';
import 'package:lodge/data/repositories/payment_option_repository.dart';
import 'package:lodge/data/repositories/receipt_repository.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/data/state/payment_option_state.dart';
import 'package:lodge/utils/number_format.dart';
import 'package:pay/pay.dart';

class Payment extends StatelessWidget {
  final BillModel bill;
  final repository = ReceiptRepository();

  Payment({Key? key, required this.bill}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Pay"),
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        foregroundColor: Colors.white,
        centerTitle: true,
        leading: const CloseButton(),
      ),
      body: SizedBox(
        width: double.infinity,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              padding: const EdgeInsets.symmetric(vertical: 15),
              child: Column(
                children: [
                  const Text("TOTAL PAYMENT",
                      style: TextStyle(color: Colors.grey, fontSize: 12)),
                  const SizedBox(height: 5),
                  Text(
                    "\$" + formatMoney(bill.amount),
                    style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 18,
                        color: Theme.of(context).primaryColor),
                  )
                ],
              ),
            ),
            Container(
              width: double.infinity,
              padding: const EdgeInsets.all(15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  const Text("Payment Options",
                      style: TextStyle(fontWeight: FontWeight.bold)),
                  const SizedBox(
                    height: 15,
                  ),
                  BlocProvider<PaymentOptionCubit>(
                    create: (context) => PaymentOptionCubit(
                        repository: PaymentOptionRepository()),
                    child: BlocBuilder<PaymentOptionCubit, PaymentOptionState>(
                        builder: (context, state) {
                      if (state is LoadingPaymentOptionState) {
                        return const Center(child: CircularProgressIndicator());
                      } else if (state is ErrorPaymentOptionState) {
                        return Center(child: Text(state.error.toString()));
                      } else if (state is LoadedPaymentOptionState) {
                        final paymentOptions = state.paymentOptions;

                        final googlePayIndex = paymentOptions.indexWhere(
                            (element) => element.name == "GooglePay");
                        final paymentOptionGooglePay =
                            paymentOptions[googlePayIndex];
                        paymentOptions.removeAt(googlePayIndex);

                        paymentOptions.add(paymentOptionGooglePay);
                        return ListView.builder(
                            shrinkWrap: true,
                            itemCount: paymentOptions.length * 2,
                            itemBuilder: (context, index) {
                              if (index.isOdd) return const Divider();
                              final indexTrue = index ~/ 2;
                              if (paymentOptions[indexTrue].name ==
                                  "GooglePay") {
                                return GooglePayButton(
                                  paymentConfigurationAsset:
                                      "default_payment_profile_google_pay.json",
                                  onPaymentResult: (res) async {
                                    final res = await repository.postFirestore(
                                        ReceiptModel(
                                            bill: bill,
                                            date: Timestamp.now(),
                                            paymentOption:
                                                paymentOptions[indexTrue]));
                                    await _showDialog(context, res);
                                  },
                                  paymentItems: [
                                    PaymentItem(
                                        label: 'Total',
                                        amount: bill.amount.toString(),
                                        status: PaymentItemStatus.final_price)
                                  ],
                                  style: GooglePayButtonStyle.flat,
                                  type: GooglePayButtonType.pay,
                                );
                              }
                              return _PaymentOption(
                                paymentOption: paymentOptions[indexTrue],
                                pay: () async {
                                  ConnectivityCubit connection =
                                      BlocProvider.of<ConnectivityCubit>(
                                          context);

                                  if (connection.state is Connected) {
                                    final res = await repository.postFirestore(
                                        ReceiptModel(
                                            bill: bill,
                                            date: Timestamp.now(),
                                            paymentOption:
                                                paymentOptions[indexTrue]));
                                    await _showDialog(context, res);
                                  } else {
                                    ScaffoldMessenger.of(context)
                                        .removeCurrentSnackBar();
                                    ScaffoldMessenger.of(context)
                                        .showSnackBar(const SnackBar(
                                      content: Text("No internet connection"),
                                      backgroundColor: Colors.red,
                                    ));
                                  }
                                },
                              );
                            });
                      } else {
                        return Container();
                      }
                    }),
                  )
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

Future<void> _showDialog(BuildContext context, bool success) async {
  final onError = {
    "icon": Icons.error,
    "color": Colors.red,
    "title": "Something happened!",
    "content": "Try again!"
  };

  final onSuccess = {
    "icon": Icons.done,
    "color": Colors.green,
    "title": "Successful payment!",
    "content": "Check your receipt now!"
  };

  final Map<String, dynamic> data;
  if (success) {
    data = onSuccess;
  } else {
    data = onError;
  }

  return showDialog(
      context: context,
      builder: (BuildContext context) => AlertDialog(
            actionsAlignment: MainAxisAlignment.center,
            title: Icon(data["icon"], color: data["color"], size: 50),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(data["title"],
                    style: const TextStyle(fontWeight: FontWeight.bold)),
                const SizedBox(height: 10),
                Text(data["content"]),
              ],
            ),
            actions: <Widget>[
              TextButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  if (success) Navigator.of(context).pop();
                },
                style: TextButton.styleFrom(backgroundColor: data["color"]),
                child: const Text(
                  'OK',
                  style: TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ),
              ),
            ],
          ));
}

class _PaymentOption extends StatelessWidget {
  final PaymentOptionModel paymentOption;
  final void Function() pay;

  const _PaymentOption({required this.paymentOption, required this.pay});

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: pay,
      child: SizedBox(
          height: 60,
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              CircleAvatar(
                  backgroundColor: Colors.black12,
                  child: Icon(
                    paymentOption.icon,
                    color: Colors.black,
                  )),
              Expanded(
                  child: Container(
                padding: const EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(paymentOption.name,
                        style: const TextStyle(fontWeight: FontWeight.bold)),
                    const SizedBox(
                      height: 5,
                    ),
                    Expanded(
                      child: Text(paymentOption.description,
                          style: const TextStyle(fontSize: 10)),
                    )
                  ],
                ),
              ))
            ],
          )),
    );
  }
}
