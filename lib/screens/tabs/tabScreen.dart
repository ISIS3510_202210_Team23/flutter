import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/tab_registering_cubit.dart';
import 'package:lodge/screens/chat-bot/chat_bot.dart';
import 'package:lodge/screens/payment_main/payment_main.dart';
import 'package:lodge/screens/profile/profle.dart';
import 'package:lodge/screens/residential_complex/residential_complex.dart';
import '../../data/cubits/user_mgt_cubit.dart';

class TabsWrapper extends StatelessWidget {
  final Map<String, dynamic> geoData;
  const TabsWrapper({ Key? key, required this.geoData }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<TabCubit>(create: (context) => TabCubit(connectionCubit: BlocProvider.of<ConnectivityCubit>(context)),
      child: Tabs(geoData: geoData),
    );
  }
}
class Tabs extends StatefulWidget {
  final Map<String, dynamic> geoData;

  const Tabs({Key? key, required this.geoData}) : super(key: key);

  @override
  State<Tabs> createState() => _TabsState();
}

class _TabsState extends State<Tabs> {
  int _index = 0;
  int _previousTab = 0;
  
  final List<String> _pages = [
    "Home",
    "Profile",
    "Residential Complex",
    "Payments",
    "Smart Assistant"
  ];

  Future<bool> onWillPop() async{
    if(_index != _previousTab){
      setState(() {
        _index = _previousTab;
        _previousTab = 0;
      });
      return false;
    }
    else{
      return true;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      child: Scaffold(
      appBar: AppBar(
        title: Text(_pages[_index]),
        elevation: 0,
        backgroundColor: Theme.of(context).primaryColor,
        foregroundColor: Colors.white,
        centerTitle: true,
        actions: [
          if(_index == 1)
            IconButton(
              onPressed: (() => context.read<UserMgtCubit>().signOut()), 
              icon: const Icon(Icons.output))
        ],
      ),
      body: IndexedStack(
        children: [
          Center(child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              const Icon(Icons.home),
              Text("${widget.geoData["latitude"]}, ${widget.geoData["longitude"]}"),
              Text(widget.geoData["inside"]),
            ],
          )),
            const Profile(),
            const ResidentialComplex(),
            const PaymentMain(),
            const ChatBot(),
          ],
          index: _index,
        ),
      bottomNavigationBar: BottomNavigationBar(
          onTap: (value) {
            var beforeValue = _index;
            setState(() {
              _index = value;
              _previousTab = beforeValue;
            });
            context.read<TabCubit>().setTabStats(_pages[value], _pages[beforeValue]);
          },
          type: BottomNavigationBarType.fixed,
          landscapeLayout: BottomNavigationBarLandscapeLayout.centered,
          selectedItemColor: Theme.of(context).primaryColor,
          unselectedItemColor: Theme.of(context).primaryColor,
          currentIndex: _index,
          showSelectedLabels: false,
          showUnselectedLabels: false,
          items: [
            BottomNavigationBarItem(
                icon: const Icon(Icons.home),
                label: "Home",
                activeIcon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Icon(Icons.home),
                    Icon(Icons.circle, size: 5)
                  ],
                )),
            BottomNavigationBarItem(
              icon: const Icon(Icons.person),
              label: "Profile",
              activeIcon: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: const [
                  Icon(Icons.person),
                  Icon(Icons.circle, size: 5)
                ],
              ),
            ),
            BottomNavigationBarItem(
                icon: const Icon(Icons.business),
                label: "Complex",
                activeIcon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Icon(Icons.business),
                    Icon(Icons.circle, size: 5)
                  ],
                )),
            BottomNavigationBarItem(
                icon: const Icon(Icons.credit_card_rounded),
                label: "Payments",
                activeIcon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Icon(Icons.credit_card_rounded),
                    Icon(Icons.circle, size: 5)
                  ],
                )),
            BottomNavigationBarItem(
                icon: const Icon(Icons.smart_toy_outlined),
                label: "bot",
                activeIcon: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: const [
                    Icon(Icons.smart_toy_outlined),
                    Icon(Icons.circle, size: 5)
                  ],
                )),
          ]),
    ), 
      onWillPop: onWillPop); 
  }
}
