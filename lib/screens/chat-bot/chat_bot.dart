import 'package:flutter/material.dart';
import 'package:dialog_flowtter/dialog_flowtter.dart';
import 'package:bubble/bubble.dart';
import 'dart:math';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/chatbot_cubit.dart';
import 'package:lodge/data/models/coneccion.dart';
import '../../data/cubits/connectivity_cubit.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../data/state/connectivity_state.dart';

class ChatBot extends StatefulWidget {
  const ChatBot({Key? key}) : super(key: key);
  @override
  ChatBotState createState() => ChatBotState();
}

class ChatBotState extends State<ChatBot> {
  @override
  void initState() {
    super.initState();

    _loadMessage();
  }

  String status = "";
  var messages = <Map>[];
  var message = "";

  late DialogFlowtter? instance;
  void response(query) async {
    DialogAuthCredentials credentials =
        await DialogAuthCredentials.fromFile('assets/service.json');

    instance = DialogFlowtter(
        credentials: credentials, sessionId: Random().nextInt(1000).toString());
    final QueryInput queryInput = QueryInput(
      text: TextInput(
        text: query,
        languageCode: "en",
      ),
    );
    DetectIntentResponse response = await instance!.detectIntent(
      queryInput: queryInput,
    );

    setState(() {
// fijar valor
      _ultimoMensaje(response.text);

      messages.insert(0, {"data": 0, "message": response.text});
    });
  }

  final messageInsert = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ChatBotCubit>(
        create: ((context) {
          if (BlocProvider.of<ChatBotCubit>(context).state.status ==
              "Connection") {
            messages
                .insert(0, {"data": 0, "message": "No internet connection "});
            context.read<ChatBotCubit>().resetStateStatus();
          }

          return ChatBotCubit(
              connectionCubit: BlocProvider.of<ConnectivityCubit>(context));
        }),
        child: Container(
          child: Column(
            children: <Widget>[
              Flexible(
                  child: ListView.builder(
                      reverse: true,
                      itemCount: messages.length,
                      itemBuilder: (context, index) => chat(
                          messages[index]["message"].toString(),
                          messages[index]["data"]))),
              const Divider(
                height: 5.0,
                color: Colors.blue,
              ),
              Container(
                padding: const EdgeInsets.only(left: 15.0, right: 15.0),
                margin: const EdgeInsets.symmetric(horizontal: 8.0),
                child: Row(
                  children: <Widget>[
                    Flexible(
                        child: TextField(
                      controller: messageInsert,
                      decoration: const InputDecoration.collapsed(
                          hintText: "Send your message",
                          hintStyle: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 18.0)),
                    )),
                    Container(
                      margin: const EdgeInsets.symmetric(horizontal: 4.0),
                      child: IconButton(
                          icon: Icon(
                            Icons.send,
                            size: 30.0,
                            color: Theme.of(context).primaryColor,
                          ),
                          onPressed: () {
                            if (messageInsert.text.isEmpty) {
                            } else {
                              setState(() {
                                messages.insert(0,
                                    {"data": 1, "message": messageInsert.text});
                              });
                              response(messageInsert.text);
                              messageInsert.clear();
                            }
                          }),
                    )
                  ],
                ),
              ),
              const SizedBox(
                height: 15.0,
              )
            ],
          ),
        ));
  }

  //for better one i have use the bubble package check out the pubspec.yaml

  Widget chat(String message, int data) {
    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Bubble(
          radius: const Radius.circular(15.0),
          color: data == 0
              ? Theme.of(context).secondaryHeaderColor
              : Theme.of(context).primaryColor,
          elevation: 0.0,
          alignment: data == 0 ? Alignment.topLeft : Alignment.topRight,
          nip: data == 0 ? BubbleNip.leftBottom : BubbleNip.rightTop,
          child: Padding(
            padding: const EdgeInsets.all(2.0),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Flexible(
                    child: Text(
                  message,
                  style: const TextStyle(
                      color: Colors.white, fontWeight: FontWeight.bold),
                ))
              ],
            ),
          )),
    );
  }

  //Cargando el valor del contador en el inicio
  _loadMessage() async {
    // SharedPreferences.setMockInitialValues({});

    SharedPreferences prefs = await SharedPreferences.getInstance();

    setState(() {
      message = (prefs.getString('lastMessage') ??
          "Hi i Am your Lodge assistant, how can i help you? ");

      messages = [
        {"data": 0, "message": "${message}"},
      ];
    });
  }

  //Incrementando el contador después del clic
  _ultimoMensaje(text) async {
    // SharedPreferences.setMockInitialValues({});
    message = text;
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      message = (prefs.getString('lastMessage') ?? "HOLA");
      prefs.setString('lastMessage', text);
    });

    print(prefs.getString('lastMessage'));
  }
}
