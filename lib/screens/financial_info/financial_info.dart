import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/financial_info_cubit.dart';
import 'package:lodge/data/models/financial_info_model.dart';
import 'package:lodge/data/state/financial_info_state.dart';
import 'package:syncfusion_flutter_charts/charts.dart';

class FinancialInfo extends StatelessWidget {
  const FinancialInfo({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Dashboard"),
          elevation: 0,
          backgroundColor: Theme.of(context).primaryColor,
          foregroundColor: Colors.white,
          centerTitle: true,
          leading: const CloseButton(),
        ),
        body: BlocProvider<FinancialInfoCubit>(
          create: (context) => FinancialInfoCubit(
              connectionCubit: BlocProvider.of<ConnectivityCubit>(context)),
          child: BlocBuilder<FinancialInfoCubit, FinancialInfoState>(
              builder: (context, state) {
            if (state is LoadingFinancialInfoState) {
              return const Center(child: CircularProgressIndicator());
            } else if (state is ErrorFinancialInfoState) {
              return Center(child: Text(state.error.toString()));
            } else if (state is FinancialInfoLoadedState) {
              return _FinancialInfoView(
                  financialInfoModel: state.financialInfo);
            } else {
              return Container();
            }
          }),
        ));
  }
}

class _FinancialInfoView extends StatelessWidget {
  final FinancialInfoModel financialInfoModel;
  final List<_ChartData> chartData = [];

  _FinancialInfoView({Key? key, required this.financialInfoModel})
      : super(key: key) {
    chartData.add(
        _ChartData(label: "Paid", value: financialInfoModel.paidPercentage));
    chartData.add(_ChartData(
        label: "Unpaid", value: financialInfoModel.unpaidPercentage));
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Column(
          children: [
            Container(
              height: 300,
              color: Theme.of(context).primaryColor,
              alignment: Alignment.center,
              child: SfCircularChart(series: <CircularSeries>[
                PieSeries<_ChartData, String>(
                    dataSource: chartData,
                    xValueMapper: (_ChartData data, _) => data.label,
                    yValueMapper: (_ChartData data, _) => data.value,
                    dataLabelMapper: (_ChartData data, _) => data.label,
                    dataLabelSettings: const DataLabelSettings(
                        isVisible: true,
                        labelPosition: ChartDataLabelPosition.outside,
                        // Renders background rectangle and fills it with series color
                        useSeriesColor: true))
              ]),
            ),
            Container(
              height: 40,
              color: Theme.of(context).primaryColor,
            )
          ],
        ),
        Container(
          padding: const EdgeInsets.all(15),
          child: Column(
            children: [
              const SizedBox(height: 300),
              IntrinsicHeight(
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: [
                    Expanded(
                      flex: 2,
                      child: Column(
                        children: [
                          Card(
                            height: 75,
                            width: double.infinity,
                            child: Row(
                              children: [
                                const Expanded(
                                    child: Text(
                                  "Total Bills",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                      color: Colors.grey),
                                )),
                                Expanded(
                                    child: Text(
                                        financialInfoModel.totalBills
                                            .toString(),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 32)))
                              ],
                            ),
                          ),
                          const SizedBox(
                            height: 15,
                          ),
                          Card(
                            height: 75,
                            width: double.infinity,
                            child: Row(
                              children: [
                                const Expanded(
                                    child: Text(
                                  "Bills paid",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15,
                                      color: Colors.grey),
                                )),
                                Expanded(
                                    child: Text(
                                        financialInfoModel.billsPaid.toString(),
                                        textAlign: TextAlign.center,
                                        style: const TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 32)))
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    const SizedBox(width: 15),
                    Expanded(
                      child: Card(
                        child: Column(
                          children: [
                            const Expanded(
                                child: Text(
                              "Completed Payments",
                              style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.grey,
                                  fontSize: 15),
                            )),
                            Expanded(
                                flex: 2,
                                child: Center(
                                  child: Text(
                                      financialInfoModel.paidPercentageString,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 36)),
                                ))
                          ],
                        ),
                      ),
                    )
                  ],
                ),
              )
            ],
          ),
        )
      ],
    );
  }
}

class _ChartData {
  _ChartData({required this.label, required this.value});

  final String label;
  final double value;
}

class Card extends StatelessWidget {
  final double? height;
  final double? width;
  final Widget child;

  const Card({
    Key? key,
    this.height,
    this.width,
    required this.child,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
        elevation: 10,
        borderRadius: BorderRadius.circular(15),
        child: Container(
          padding: const EdgeInsets.all(15),
          height: height,
          width: width,
          child: child,
        ));
  }
}
