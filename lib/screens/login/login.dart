import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/loginCubit.dart';
import 'package:lodge/data/models/login.dart';

class LoginScreen extends StatelessWidget {
  const LoginScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            physics: const NeverScrollableScrollPhysics(),
            child: BlocProvider(
                create: ((context) => LoginCubit(
                    connectionCubit:
                        BlocProvider.of<ConnectivityCubit>(context))),
                child: BlocConsumer<LoginCubit, LoginState>(
                  listenWhen: ((previous, current) =>
                      previous.status != current.status),
                  listener: ((context, state) {
                    if (state.status != "") {
                      ScaffoldMessenger.of(context).removeCurrentSnackBar();
                    }
                    if (state.status == "LoggedIn") {
                      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                          content: Text("Success"),
                          backgroundColor: Colors.green));
                    } else if (state.status == "Error") {
                      ScaffoldMessenger.of(context).showSnackBar( SnackBar(
                          content: Text(state.message??"An unexpected error has ocurred, please try again later"),
                          backgroundColor: Colors.red));
                    } else if (state.status == "Connection") {
                      ScaffoldMessenger.of(context)
                          .showSnackBar(const SnackBar(
                            content: Text(
                                "Login failed, check your internet connection"),
                            backgroundColor: Colors.red,
                            duration: Duration(seconds: 3),
                          ))
                          .closed
                          .then((value) =>
                              context.read<LoginCubit>().resetStateStatus());
                    }
                  }),
                  builder: (context, state) => ConstrainedBox(
                    constraints: BoxConstraints(
                        minHeight: MediaQuery.of(context).size.height,
                        minWidth: MediaQuery.of(context).size.width),
                    child: IntrinsicHeight(
                        child: Container(
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          Container(
                            height: MediaQuery.of(context).size.height * 0.3,
                            child: Column(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Image.asset('assets/images/mainLogo.png'),
                                  Text("Lodge",
                                      style: GoogleFonts.mPlus1(
                                          textStyle: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: Theme.of(context)
                                                  .primaryColor,
                                              fontSize: 22)))
                                ]),
                            alignment: Alignment.bottomCenter,
                          ),
                          Flexible(
                              fit: FlexFit.loose,
                              child: Padding(
                                  padding: const EdgeInsets.only(top: 20),
                                  child: Column(children: <Widget>[
                                    const LoginForm(),
                                    GestureDetector(
                                      onTap: () {
                                        // context.read<LoginCubit>().googleLogin();
                                      },
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.center,
                                        children: [
                                          Image.asset(
                                              'assets/images/googleLogo.png'),
                                          const SizedBox(width: 10),
                                          const Text("Login with Google")
                                        ],
                                      ),
                                    )
                                  ]))),
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text(
                                  "Don't have an account? ",
                                  style: GoogleFonts.mPlus1(
                                      textStyle:
                                          const TextStyle(color: Colors.black)),
                                ),
                                TextButton(
                                  onPressed: () {
                                    Navigator.pushNamed(context, '/register');
                                  },
                                  child: Text("Sign up",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color:
                                              Theme.of(context).primaryColor)),
                                  style: ButtonStyle(
                                      overlayColor: MaterialStateProperty.all(
                                          Colors.transparent),
                                      padding: MaterialStateProperty.all(
                                          const EdgeInsets.all(0))),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                      margin: const EdgeInsets.only(left: 50, right: 50),
                      height: MediaQuery.of(context).size.height,
                    )),
                  ),
                ))));
  }
}

class LoginForm extends StatefulWidget {
  const LoginForm({Key? key}) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  final _formKey = GlobalKey<FormState>();

  String? _validate(String? value, String field) {
    if (field == "email") {
      if (value == null || value.isEmpty || value.trim() == "") {
        return "Email required";
      } else if (RegExp(
              r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$')
          .hasMatch(value)) {
        return null;
      } else {
        return "Please use a valid email";
      }
    } else {
      if (value == null || value.isEmpty || value.trim() == "") {
        return "Pasword required";
      } else {
        return null;
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _formKey,
        child: Column(
          children: <Widget>[
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              onChanged: (value) {
                context.read<LoginCubit>().onChangeEmail(value);
              },
              validator: (value) {
                return _validate(value, "email");
              },
              decoration: const InputDecoration(
                  labelText: "Username", labelStyle: TextStyle(fontSize: 12)),
            ),
            TextFormField(
              autovalidateMode: AutovalidateMode.onUserInteraction,
              obscureText: true,
              enableSuggestions: false,
              autocorrect: false,
              onChanged: (value) {
                context.read<LoginCubit>().onPasswordChange(value);
              },
              validator: (value) {
                return _validate(value, "password");
              },
              decoration: const InputDecoration(
                  labelText: "Password", labelStyle: TextStyle(fontSize: 12)),
            ),
            const SizedBox(
              height: 10,
            ),
            Padding(
                padding: const EdgeInsets.only(top: 10, bottom: 10),
                child: ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      context.read<LoginCubit>().onSubmit();
                    }
                  },
                  child: const Text("Login",
                      style: TextStyle(color: Colors.white)),
                  style: ElevatedButton.styleFrom(
                      padding: const EdgeInsets.only(left: 30, right: 30),
                      shape: const StadiumBorder(),
                      elevation: 0,
                      primary: Theme.of(context).primaryColor),
                )),
          ],
        ));
  }
}
