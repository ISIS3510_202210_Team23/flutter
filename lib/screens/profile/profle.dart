import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/profile_cubit.dart';
import 'package:lodge/data/state/profile_state.dart';

class Profile extends StatelessWidget {
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ProfileCubit>(
        create: (context) => ProfileCubit(connectionCubit: BlocProvider.of<ConnectivityCubit>(context)),
        child: BlocBuilder<ProfileCubit, ProfileState>(builder: (context, state) {
          if(state is LoadingProfileState){
            return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor),
                    ),
                  );
          }
          else if(state is ErrorProfileState){
            return Center(
              child: Text(state.message, textAlign: TextAlign.center,)
            );
          }
          else if (state is LoadedProfileState){
            return SingleChildScrollView(
              child: Padding(
            padding: const EdgeInsets.only(left: 20, right: 20),
            child: Column(
              children: [
                const SizedBox(
                  height: 40,
                ),
                CachedNetworkImage(
                    key: ValueKey(state.model.image),
                    imageUrl: state.model.image,
                    placeholder: (context, url) {
                      return Container(
                        height: 70,
                        width: 70,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color.fromRGBO(0, 27, 60, 1),
                        ),
                        child: const Icon(
                          Icons.person_rounded,
                          size: 50,
                          color: Colors.white,
                        ),
                      );
                    },
                    errorWidget: (context, url, error) {
                      return Container(
                        height: 70,
                        width: 70,
                        decoration: const BoxDecoration(
                          shape: BoxShape.circle,
                          color: Color.fromRGBO(0, 27, 60, 1),
                        ),
                        child: const Icon(
                          Icons.person_rounded,
                          size: 50,
                          color: Colors.white,
                        ),
                      );
                    },
                    fit: BoxFit.cover,
                    height: 70,
                    width: 70,
                ),
                Text(
                  state.model.name,
                  style: const TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
                  overflow: TextOverflow.ellipsis,
                ),
                const SizedBox(
                  height: 20,
                ),
                const Align(
                  alignment: Alignment.centerLeft,
                  child: Text("Basic Info",
                      style:
                          TextStyle(fontWeight: FontWeight.bold, fontSize: 15)),
                ),
                Padding(
                    padding: const EdgeInsets.only(left:5, right: 5, top: 20, bottom: 20),
                    child: Table(
                      children: [
                        TableRow(
                          children: [
                          TableCell(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10, right: 20), 
                              child: Row(
                              children: [
                                const LabelText(value: "Name"),
                                const SizedBox(
                                  width: 10,
                                ),
                                TextInfo(value: state.model.name)
                              ],
                              mainAxisAlignment: MainAxisAlignment.start,
                            )),
                          ),
                          TableCell(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10, right: 20), 
                            child: Row(children: [
                              const LabelText(value: "House"),
                              const SizedBox(
                                width: 10,
                              ),
                              TextInfo(value: state.model.house)
                            ], 
                            mainAxisAlignment: MainAxisAlignment.start),
                          ))
                        ]),
                        TableRow(children: [
                          TableCell(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10, right: 20), 
                              child: Row(
                              children: [
                                const LabelText(value: "Email"),
                                const SizedBox(
                                  width: 10,
                                ),
                                TextInfo(value: state.model.email)
                              ],
                              mainAxisAlignment: MainAxisAlignment.start,
                            ),)
                          ),
                          TableCell(
                            child: Padding(
                              padding: const EdgeInsets.only(left: 10, right: 20), 
                            child: Row(children: [
                              const LabelText(value: "Phone"),
                              const SizedBox(
                                width: 10,
                              ),
                              TextInfo(value: state.model.phone)
                            ], 
                            mainAxisAlignment: MainAxisAlignment.start,),
                          ))
                        ])
                      ],
                    )),
                const Align(
                    alignment: Alignment.centerLeft,
                    child: Text("Hommies",
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 15))),
                const SizedBox(
                  height: 10,
                ),
                const Text("No Hommies yet")
              ],
            ),
          ));
          }
          else{
            return Container();
          }
        }));
  }
}

class LabelText extends StatelessWidget {
  final String value;
  const LabelText({ Key? key, required this.value }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Text(value,
      style: const TextStyle(
        fontWeight: FontWeight.bold,
        fontSize: 12,
      ),
    );
  }
}

class TextInfo extends StatelessWidget {
  final String value;
  const TextInfo({ Key? key, required this.value }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child:Text(value,
        style: const TextStyle(
          fontWeight: FontWeight.w200,
          fontSize: 12,
        ),
        overflow: TextOverflow.ellipsis,
      )
    );
  }
}
