import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/residential_complex_cubit.dart';

class TakePictureResidentialComplex extends StatefulWidget {
  const TakePictureResidentialComplex(
      {Key? key, required this.camera, required this.treeContext})
      : super(key: key);

  final CameraDescription camera;
  final BuildContext treeContext;

  @override
  State<StatefulWidget> createState() => _TakePictureResidentialComplexState();
}

class _TakePictureResidentialComplexState
    extends State<TakePictureResidentialComplex> with WidgetsBindingObserver{
  late CameraController _controller;
  late Future<void> _initializeControllerFuture;

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    if(!_controller.value.isInitialized || _controller == null){
      return;
    }
    if(state == AppLifecycleState.inactive){
      _controller.dispose();
    }
    else if(state == AppLifecycleState.resumed){
      _controller = CameraController(widget.camera, ResolutionPreset.high, enableAudio: false);
      onStartCamera();
    }
  }

  void onStartCamera(){
    _initializeControllerFuture = _controller.initialize()
    .then((value) => null).catchError((e){
      if(e is CameraException){
        switch (e.code) {
          case 'CameraAccessDenied':
              showDialog(context: context, builder: (BuildContext context) => AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Center(child: Text("Camera access denied by the user")),
                  ],
                ),
                actionsAlignment: MainAxisAlignment.center,
                actions: [
                  TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("OK"))
                ],
              ));
            break;
          case 'cameraPermission':
              showDialog(context: context, builder: (BuildContext context) => AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Center(child: Text("Enable camera to edit picture")),
                  ],
                ),
                actionsAlignment: MainAxisAlignment.center,
                actions: [
                  TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("OK"))
                ],
              ));
            break;
          default:
              showDialog(context: context, builder: (BuildContext context) => AlertDialog(
                content: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Center(child: Text("Could not open camera")),
                  ],
                ),
                actionsAlignment:  MainAxisAlignment.center,
                actions: [
                  TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("OK"))
                ],
              ));
            break;
        }
      }
    });
  }

  @override
  void initState() {
    super.initState();

    WidgetsBinding.instance?.addObserver(this);
    _controller = CameraController(widget.camera, ResolutionPreset.high, enableAudio: false);
    onStartCamera();
  }

  @override
  void dispose() {
    WidgetsBinding.instance?.removeObserver(this);
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<void>(
          future: _initializeControllerFuture,
          builder: (context, snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                return CameraPreview(_controller);
              } else {
                return const Center(child: CircularProgressIndicator());
              }
          },
        ),
        floatingActionButton: FloatingActionButton(
            onPressed: () async {
              try {
                // Ensure that the camera is initialized.
                await _initializeControllerFuture;

                // Attempt to take a picture and get the file `image`
                // where it was saved.
                final image = await _controller.takePicture();
                BlocProvider.of<ResComplexCubit>(widget.treeContext)
                    .postBanner(image.path);
                Navigator.pop(context);
              } catch (e) {
                // If an error occurs, log the error to the console.
                print(e);
              }
            },
            child: const Icon(Icons.camera_alt)));
  }
}
