import 'package:cached_network_image/cached_network_image.dart';
import 'package:camera/camera.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/residential_complex_cubit.dart';
import 'package:lodge/data/models/complexModel.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/data/state/residential_complex_state.dart';
import 'package:url_launcher/url_launcher.dart';

class ResidentialComplex extends StatefulWidget {
  const ResidentialComplex({Key? key}) : super(key: key);

  @override
  State<ResidentialComplex> createState() => _ResidentialComplexState();
}

class _ResidentialComplexState extends State<ResidentialComplex> {
  int selected = 0;

  void changeSelected(index) {
    setState(() {
      selected = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: Container(
        padding: const EdgeInsets.all(15),
        child: BlocProvider<ResComplexCubit>(
            create: (context) => ResComplexCubit(
                connectionCubit: BlocProvider.of<ConnectivityCubit>(context)),
            child: BlocBuilder<ResComplexCubit, ResidentialComplexState>(
              builder: (context, state) {
                if (state is LoadingResComplexState) {
                  return Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                          Theme.of(context).primaryColor),
                    ),
                  );
                } else if (state is LoadedResComplexState) {
                  return Column(children: [
                    _Banner(
                      complexInfo: state.complexModel,
                    ),
                    const SizedBox(height: 20),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        _Button(
                            name: "Residents",
                            iconName: Icons.person,
                            selected: selected == 0,
                            callback: () {
                              changeSelected(0);
                            }),
                        _Button(
                            name: "Guests",
                            iconName: Icons.people,
                            selected: selected == 1,
                            callback: () {
                              changeSelected(1);
                            }),
                        _Button(
                            name: "Finances",
                            iconName: Icons.attach_money,
                            selected: false,
                            callback: () {
                              Navigator.pushNamed(context, "/financialInfo");
                            }),
                      ],
                    ),
                    if (selected == 0)
                      AdministratorInfo(
                        complexInfo: state.complexModel,
                      )
                    else
                      const Center(child: Text("Nothing here"))
                  ]);
                } else {
                  return const Center(
                      child: Text("Failed to get data, pull down to refresh"));
                }
              },
            )))
    );
  }
}

class _Banner extends StatelessWidget {
  final ComplexModel complexInfo;

  const _Banner({Key? key, required this.complexInfo}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 200,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Stack(
          children: [
            Row(
              children: [
                Expanded(
                  child: CachedNetworkImage(
                    key: ValueKey(complexInfo.image),
                    imageUrl: complexInfo.image ?? "",
                    placeholder: (context, url) {
                      return Image.asset(
                        'assets/images/complexGeneric.png',
                        fit: BoxFit.cover,
                        height: double.infinity,
                      );
                    },
                    errorWidget: (context, url, error) {
                      return Image.asset(
                        'assets/images/complexGeneric.png',
                        fit: BoxFit.cover,
                        height: double.infinity,
                      );
                    },
                    fit: BoxFit.cover,
                    height: double.infinity,
                  ),
                )
              ],
            ),
            Container(
              padding: const EdgeInsets.all(10),
              decoration: BoxDecoration(
                  gradient: LinearGradient(
                      begin: Alignment.bottomCenter,
                      end: Alignment.topCenter,
                      colors: [
                    Colors.black.withOpacity(0.35),
                    Colors.black.withOpacity(0)
                  ],
                      stops: const [
                    0.40,
                    1,
                  ])),
              child: Row(
                children: [
                  Expanded(
                    child: Column(
                      verticalDirection: VerticalDirection.up,
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                        Flexible(
                            child: Text("Address " + (complexInfo.address),
                                style: const TextStyle(
                                  color: Colors.white,
                                ))),
                        Text(complexInfo.name,
                            style: const TextStyle(
                                color: Colors.white,
                                fontWeight: FontWeight.bold)),
                      ],
                    ),
                  ),
                  Column(
                      verticalDirection: VerticalDirection.up,
                      crossAxisAlignment: CrossAxisAlignment.end,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        const Icon(
                          Icons.map,
                          color: Colors.white,
                        ),
                        GestureDetector(
                            onTap: () async {
                              if(context.read<ConnectivityCubit>().state is Disconnected || context.read<ConnectivityCubit>().state is UnknownConnectivity){
                                showDialog(context: context, builder: (BuildContext context) => AlertDialog(
                                  content: Column(
                                    mainAxisSize: MainAxisSize.min,
                                    children: const [
                                      Center(child: Text("Cannot edit picture right now, connect to the internet")),
                                    ],
                                  ),
                                  actionsAlignment: MainAxisAlignment.center,
                                  actions: [
                                    TextButton(onPressed: ()=>Navigator.pop(context), child: const Text("OK"))
                                  ],
                                ));
                                return;
                              }
                              final cameras = await availableCameras();
                              Navigator.pushNamed(context, "/takePicture",
                                  arguments: {
                                    "camera": cameras.first,
                                    "treeContext": context
                                  });
                            },
                            child: const Icon(Icons.edit, color: Colors.white)),
                      ]),
                ],
              ),
            )
          ],
        ),
      ),
    );
  }
}

class AdministratorInfo extends StatelessWidget {
  final ComplexModel complexInfo;

  const AdministratorInfo({Key? key, required this.complexInfo})
      : super(key: key);

  Future<void> _makePhoneCall(String url, context) async {
    if (url == "") {
      return;
    }
    final uri = Uri(scheme: 'tel', path: url);
    if (await canLaunchUrl(Uri(scheme: 'tel', path: url))) {
      await launchUrl(uri);
    } else {
      ScaffoldMessenger.of(context).removeCurrentSnackBar();
      ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
        content: Text("Failed to call"),
        backgroundColor: Colors.red,
      ));
    }
  }

  @override
  Widget build(BuildContext context) {
    if (complexInfo.admin != null) {
      return Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          const SizedBox(height: 20),
          const Text("Administrator",
              textAlign: TextAlign.left,
              style: TextStyle(fontWeight: FontWeight.bold)),
          const SizedBox(height: 10),
          Container(
            decoration: BoxDecoration(
                border: Border.all(color: Colors.grey, width: 0.5),
                borderRadius: BorderRadius.circular(10)),
            child: Row(
              children: [
                const Padding(
                  padding: EdgeInsets.only(right: 15, left: 15),
                  child: Icon(Icons.person),
                ),
                Expanded(
                    child: Text(
                  complexInfo.admin?.name ?? "",
                )),
                IconButton(
                    onPressed: () {
                      if (complexInfo.admin != null) {
                        _makePhoneCall(complexInfo.admin?.phone ?? "", context);
                      }
                    },
                    icon: const Icon(Icons.call, color: Colors.black, size: 22))
              ],
            ),
          )
        ],
      );
    } else {
      return Container(
        child: const Text("Nothing to show"),
      );
    }
  }
}

class _Button extends StatelessWidget {
  final String name;
  final IconData iconName;
  final bool selected;
  VoidCallback callback;

  _Button({
    Key? key,
    required this.name,
    required this.iconName,
    required this.selected,
    required this.callback,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ElevatedButton(
      onPressed: () {
        callback();
      },
      child: Column(
        children: [
          Icon(iconName,
              size: 48,
              color:
                  (selected ? Colors.white : Theme.of(context).primaryColor)),
          Text(
            name,
            style: TextStyle(
                color:
                    selected ? Colors.white : Theme.of(context).primaryColor),
          )
        ],
      ),
      style: ElevatedButton.styleFrom(
          primary: selected ? Theme.of(context).primaryColor : Colors.white,
          minimumSize: const Size(95, 69),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          )),
      // style: ,
    );
  }
}
