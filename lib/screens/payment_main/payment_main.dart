import 'package:flutter/material.dart';
import 'package:lodge/screens/payment_main/must_pay.dart';
import 'package:lodge/screens/payment_main/paid.dart';

class PaymentMain extends StatefulWidget {
  const PaymentMain({Key? key}) : super(key: key);

  @override
  _PaymentMainState createState() => _PaymentMainState();
}

class _PaymentMainState extends State<PaymentMain> {
  int _selectedPage = 0;
  final _controller = PageController(initialPage: 0);

  void _changePage(int pageNum) {
    setState(() {
      _selectedPage = pageNum;
      _controller.animateToPage(pageNum,
          duration: const Duration(milliseconds: 500),
          curve: Curves.fastLinearToSlowEaseIn);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Container(
          margin: const EdgeInsets.all(15),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20), color: Colors.grey),
          child: Row(
            children: [
              Expanded(
                  child: _TabButton(
                text: "Must Pay",
                selectedPage: _selectedPage,
                pageNumber: 0,
                onPressed: () {
                  _changePage(0);
                },
              )),
              Expanded(
                  child: _TabButton(
                text: "Paid",
                selectedPage: _selectedPage,
                pageNumber: 1,
                onPressed: () {
                  _changePage(1);
                },
              ))
            ],
          ),
        ),
        Expanded(
          child: PageView(
            onPageChanged: (int page) {
              setState(() {
                _selectedPage = page;
              });
            },
            controller: _controller,
            children: const [
              MustPay(),
              Paid(
                checks: [],
              )
            ],
          ),
        )
      ],
    );
  }
}

class _TabButton extends StatelessWidget {
  final String text;
  final int selectedPage;
  final int pageNumber;
  final void Function() onPressed;

  const _TabButton(
      {Key? key,
      required this.text,
      required this.selectedPage,
      required this.pageNumber,
      required this.onPressed})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onPressed,
      child: AnimatedContainer(
        duration: const Duration(milliseconds: 200),
        alignment: Alignment.center,
        height: 40,
        decoration: BoxDecoration(
            color: selectedPage == pageNumber ? Theme.of(context).secondaryHeaderColor : Colors.grey,
            borderRadius: BorderRadius.circular(20)),
        child: Text(
          text,
          style: TextStyle(
              color: Colors.white,
              fontWeight: selectedPage == pageNumber
                  ? FontWeight.bold
                  : FontWeight.normal),
        ),
      ),
    );
  }
}
