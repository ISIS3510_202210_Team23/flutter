import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/cubits/receipt_cubit.dart';
import 'package:lodge/data/models/receipt.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/data/state/receipts_state.dart';
import 'package:lodge/utils/number_format.dart';

class Paid extends StatelessWidget {
  final List<Object> checks;

  const Paid({Key? key, required this.checks}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<ReceiptCubit>(
        create: (context) => ReceiptCubit(
            connectionCubit: BlocProvider.of<ConnectivityCubit>(context)),
        child:
            BlocBuilder<ReceiptCubit, ReceiptState>(builder: (context, state) {
          if (state is LoadingReceiptState) {
            return const Center(child: CircularProgressIndicator());
          } else if (state is ErrorReceiptState) {
            return Center(child: Text(state.error.toString()));
          } else if (state is LoadedReceiptState) {
            final receipts = state.receipts;

            if (receipts.isEmpty) {
              return const Center(child: Text('No receipts found'));
            }

            return ListView.builder(
                padding: const EdgeInsets.all(15.0),
                itemCount: (receipts.length * 2) + 1,
                itemBuilder: (context, index) {
                  if (index.isOdd) return const Divider();

                  final indexTrue = index ~/ 2;
                  if (indexTrue >= receipts.length) {
                    BlocProvider.of<ReceiptCubit>(context).getReceipts();
                    return Center(
                        child: state.hasReachedMax
                            ? BlocProvider.of<ConnectivityCubit>(context).state
                                    is Connected
                                ? const Text("No more receipts")
                                : const Text("Need internet to load more")
                            : const CircularProgressIndicator());
                  }
                  return _PaidItem(receipt: receipts[indexTrue]);
                });
          } else {
            return Container();
          }
        }));
  }
}

class _PaidItem extends StatelessWidget {
  final ReceiptModel receipt;

  const _PaidItem({required this.receipt});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 50,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const CircleAvatar(
              backgroundColor: Colors.black12,
              child: Icon(
                Icons.receipt_long,
                color: Colors.black,
              )),
          Expanded(
            flex: 1,
            child: Container(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Flexible(
                    child: RichText(
                      overflow: TextOverflow.ellipsis,
                      text: TextSpan(
                        text: receipt.bill.name,
                        style: const TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    receipt.date.toDate().toString(),
                    style: const TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
            ),
          ),
          Text(
            "\$" + formatMoney(receipt.bill.amount),
          )
        ],
      ),
    );
  }
}
