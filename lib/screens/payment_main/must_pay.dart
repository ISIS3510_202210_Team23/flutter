import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:lodge/data/cubits/bill_cubit.dart';
import 'package:lodge/data/cubits/connectivity_cubit.dart';
import 'package:lodge/data/models/bill.dart';
import 'package:lodge/data/state/bills_state.dart';
import 'package:lodge/data/state/connectivity_state.dart';
import 'package:lodge/utils/number_format.dart';

class MustPay extends StatelessWidget {
  const MustPay({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<BillCubit>(
      create: (context) => BillCubit(
          connectionCubit: BlocProvider.of<ConnectivityCubit>(context)),
      child: BlocBuilder<BillCubit, BillsState>(builder: (context, state) {
        if (state is LoadingBillsState) {
          return const Center(child: CircularProgressIndicator());
        } else if (state is ErrorBillsState) {
          return Center(child: Text(state.error.toString()));
        } else if (state is LoadedBillsState) {
          final bills = state.bills;

          if (bills.isEmpty) {
            return const Center(child: Text('No bills to pay'));
          }

          return ListView.builder(
              padding: const EdgeInsets.all(15.0),
              itemCount: (bills.length * 2) + 1,
              itemBuilder: (context, index) {
                if (index.isOdd) return const Divider();

                final indexTrue = index ~/ 2;
                if (indexTrue >= bills.length) {
                  BlocProvider.of<BillCubit>(context).getBills();
                  return Center(
                      child: state.hasReachedMax
                          ? BlocProvider.of<ConnectivityCubit>(context).state
                                  is Connected
                              ? const Text("No more bills")
                              : const Text("Need internet to load more")
                          : const CircularProgressIndicator());
                }

                return _MustPayItem(bill: bills[indexTrue]);
              });
        } else {
          return Container();
        }
      }),
    );
  }
}

class _MustPayItem extends StatelessWidget {
  final BillModel bill;

  const _MustPayItem({required this.bill});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: 55,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          const CircleAvatar(
              backgroundColor: Colors.black12,
              child: Icon(
                Icons.receipt_long,
                color: Colors.black,
              )),
          Expanded(
            flex: 1,
            child: Container(
              padding: const EdgeInsets.all(8),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(children: [
                    Flexible(
                      child: RichText(
                        overflow: TextOverflow.ellipsis,
                        text: TextSpan(
                          text: bill.name,
                          style: const TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    const Material(
                      color: Colors.grey,
                      type: MaterialType.circle,
                      child: SizedBox(
                        height: 4,
                        width: 4,
                      ),
                    ),
                    const SizedBox(
                      width: 5,
                    ),
                    Text("\$" + formatMoney(bill.amount),
                        style: const TextStyle(
                          fontSize: 12,
                        ))
                  ]),
                  const SizedBox(
                    height: 5,
                  ),
                  Text(
                    bill.dueDate.toDate().toString(),
                    style: const TextStyle(fontSize: 10, color: Colors.grey),
                  )
                ],
              ),
            ),
          ),
          ElevatedButton(
              onPressed: () {
                ConnectivityCubit connection =
                    BlocProvider.of<ConnectivityCubit>(context);
                if (connection.state is Connected) {
                  Navigator.pushNamed(context, "/payment", arguments: bill)
                      .then((value) => context.read<BillCubit>().getBills());
                } else {
                  ScaffoldMessenger.of(context).removeCurrentSnackBar();
                  ScaffoldMessenger.of(context).showSnackBar(const SnackBar(
                    content: Text("No internet connection"),
                    backgroundColor: Colors.red,
                  ));
                }
              },
              style: ElevatedButton.styleFrom(
                  primary: Theme.of(context).secondaryHeaderColor,
                  minimumSize: const Size(50, 25),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18))),
              child: const Text("Pay"))
        ],
      ),
    );
  }
}
