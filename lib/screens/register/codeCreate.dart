import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lodge/data/cubits/registerCubit.dart';
import 'package:lodge/data/models/register.dart';

class ComplexCodeScreen extends StatelessWidget {
  const ComplexCodeScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Colors.white,
      child: SafeArea(
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.white,
                elevation: 0,
                centerTitle: true,
                title: Text("Complex Code",
                    style: TextStyle(
                        color: Theme.of(context).secondaryHeaderColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold)),
                leading: IconButton(
                    icon: Icon(Icons.close,
                        color: Theme.of(context).secondaryHeaderColor),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ),
              body: BlocConsumer<RegisterCubit, RegisterState>(
                  listener: ((context, state) {
                if (state.status == "Success") {
                  ScaffoldMessenger.of(context)
                      .showSnackBar(const SnackBar(content: Text("Success")));
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (_) => AlertDialog(
                            actionsAlignment: MainAxisAlignment.center,
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: const [
                                Icon(Icons.check_circle,
                                    color: Colors.green, size: 50),
                                Text(
                                  "Congratulations! you can now log in",
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.popUntil(
                                        context, ModalRoute.withName("/"));
                                  },
                                  child: const Text("OK"))
                            ],
                          ));
                  context.read<RegisterCubit>().resetState();
                } else if (state.status == "Loading") {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (_) => AlertDialog(
                          actionsAlignment: MainAxisAlignment.center,
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              CircularProgressIndicator(
                                color: Color.fromRGBO(0, 71, 137, 1),
                              )
                            ],
                          )));
                } else if (state.status != "") {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (_) => AlertDialog(
                            actionsAlignment: MainAxisAlignment.center,
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const Icon(Icons.error,
                                    color: Colors.red, size: 50),
                                Text(
                                  state.status == "Connection"
                                      ? "Cannot register at this moment, check your internet connection"
                                      : state.status,
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    if (state.status == "Connection") {
                                      Navigator.pop(context);
                                    } else {
                                      Navigator.pop(context);
                                      Navigator.pop(context);
                                    }
                                  },
                                  child: const Text("OK"))
                            ],
                          ));
                  context.read<RegisterCubit>().setFields("status", "");
                }
              }), builder: (context, state) {
                return LayoutBuilder(
                  builder: ((context, constraints) {
                    return SingleChildScrollView(
                      child: ConstrainedBox(
                        constraints: BoxConstraints(
                          minHeight: constraints.maxHeight,
                          minWidth: constraints.maxWidth,
                        ),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                                width: MediaQuery.of(context).size.width * 0.8,
                                child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Image.asset(
                                    "assets/images/Authentication-bro.png",
                                    height: 320,
                                    width: 320,
                                  ),
                                )),
                            Container(
                              margin:
                                  const EdgeInsets.only(left: 100, right: 100),
                              child: Column(
                                children: <Widget>[
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        top: 10, bottom: 20),
                                    child: Text("Enter your code",
                                        style: GoogleFonts.mPlus1(
                                            textStyle: const TextStyle(
                                                color: Colors.black,
                                                fontSize: 14,
                                                fontWeight: FontWeight.bold))),
                                  ),
                                  const ComplexCode()
                                ],
                              ),
                            )
                          ],
                        ),
                      ),
                    );
                  }),
                );
              }))),
    );
  }
}

class ComplexCode extends StatefulWidget {
  const ComplexCode({Key? key}) : super(key: key);

  @override
  State<ComplexCode> createState() => _ComplexCodeState();
}

class _ComplexCodeState extends State<ComplexCode> {
  final _ComplexCodeForm = GlobalKey<FormState>();

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _ComplexCodeForm,
      child: Column(
        children: <Widget>[
          TextFormField(
            initialValue: context.read<RegisterCubit>().state.code,
            textAlign: TextAlign.center,
            inputFormatters: [LengthLimitingTextInputFormatter(40)],
            decoration: const InputDecoration(
                hintText: "ABDC123\$XYZ",
                border: OutlineInputBorder(
                    borderRadius: BorderRadius.all(Radius.circular(10)))),
            onChanged: (value) {
              context.read<RegisterCubit>().setFields("code", value);
            },
            validator: (value) {
              if (value == null || value.isEmpty) {
                return "Please use your residential complex code";
              }
              return null;
            },
          ),
          const SizedBox(height: 10),
          ElevatedButton(
              onPressed: () {
                if (_ComplexCodeForm.currentState!.validate()) {
                    context.read<RegisterCubit>().registerUser();
                }
              },
              child: const Text("Submit"),
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.only(left: 30, right: 30),
                  shape: const StadiumBorder(),
                  elevation: 0,
                  primary: Theme.of(context).secondaryHeaderColor))
        ],
      ),
    );
  }
}

class CreateComplexScreen extends StatelessWidget {
  const CreateComplexScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        color: Colors.white,
        child: SafeArea(
          child: Scaffold(
              appBar: AppBar(
                backgroundColor: Colors.transparent,
                shadowColor: Colors.white,
                elevation: 0,
                leading: IconButton(
                    icon: Icon(Icons.close,
                        color: Theme.of(context).secondaryHeaderColor),
                    onPressed: () {
                      Navigator.pop(context);
                    }),
              ),
              body: BlocConsumer<RegisterCubit, RegisterState>(
                  listener: (context, state) {
                if (state.status == "Success") {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (_) => AlertDialog(
                            actionsAlignment: MainAxisAlignment.center,
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: const [
                                Icon(Icons.check_circle,
                                    color: Colors.green, size: 50),
                                Text("Congratulations! you can now log in")
                              ],
                            ),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.popUntil(
                                        context, ModalRoute.withName("/"));
                                  },
                                  child: const Text("OK"))
                            ],
                          ));
                  context.read<RegisterCubit>().resetState();
                } else if (state.status == "Loading") {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (_) => AlertDialog(
                          actionsAlignment: MainAxisAlignment.center,
                          content: Column(
                            mainAxisSize: MainAxisSize.min,
                            children: const [
                              CircularProgressIndicator(
                                color: Color.fromRGBO(0, 71, 137, 1),
                              )
                            ],
                          )));
                } else if (state.status != "") {
                  showDialog(
                      barrierDismissible: false,
                      context: context,
                      builder: (_) => AlertDialog(
                            actionsAlignment: MainAxisAlignment.center,
                            content: Column(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                const Icon(Icons.error,
                                    color: Colors.red, size: 50),
                                Text(
                                  state.status == "Connection"
                                      ? "Cannot register your complex at this moment, check your internet connection"
                                      : state.status,
                                  textAlign: TextAlign.center,
                                )
                              ],
                            ),
                            actions: [
                              TextButton(
                                  onPressed: () {
                                    Navigator.pop(context);
                                    Navigator.pop(context);
                                  },
                                  child: const Text("OK"))
                            ],
                          ));
                  context.read<RegisterCubit>().setFields("status", "");
                }
              }, builder: (context, state) {
                return const CreateComplexForm();
              })),
        ));
  }
}

class CreateComplexForm extends StatefulWidget {
  const CreateComplexForm({Key? key}) : super(key: key);

  @override
  State<CreateComplexForm> createState() => _CreateComplexFormState();
}

class _CreateComplexFormState extends State<CreateComplexForm> {
  final _createComplexFormKey = GlobalKey<FormState>();

  String? validation(String? value, String title) {
    if (value == null || value.isEmpty) {
      return "Please fill the required information";
    }
    if (value.trim() == "") {
      return "Please enter a valid " + title.toLowerCase();
    }
    return null;
  }

  static const fields = [
    {"title": "Name"},
    {"title": "City"},
    {"title": "Address"}
  ];

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _createComplexFormKey,
        child: Container(
          margin: const EdgeInsets.only(left: 50, right: 50, bottom: 80),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text("Create Complex",
                  style: TextStyle(
                      color: Theme.of(context).secondaryHeaderColor,
                      fontSize: 18,
                      fontWeight: FontWeight.bold)),
              const SizedBox(height: 10),
              ...fields.map((e) {
                var initialValue = "";
                if(e["title"] == "Name"){
                  initialValue = context.read<RegisterCubit>().state.complexName;
                } else if(e["title"] == "City"){
                  initialValue = context.read<RegisterCubit>().state.city;
                } else if(e["title"] == "Address"){
                  initialValue = context.read<RegisterCubit>().state.address;
                }
                return SizedBox(
                    height: 60,
                    child: TextFormField(
                      initialValue: initialValue,
                      inputFormatters: [LengthLimitingTextInputFormatter(40)],
                      decoration: InputDecoration(
                        labelText: e["title"],
                        labelStyle: const TextStyle(fontSize: 15),
                      ),
                      validator: (value) {
                        return validation(value, e["title"] ?? " input");
                      },
                      onChanged: (value) {
                        if (e["title"] == "Name") {
                          context
                              .read<RegisterCubit>()
                              .setFields("complexName", value);
                        } else if (e["title"] == "City") {
                          context
                              .read<RegisterCubit>()
                              .setFields("city", value);
                        } else {
                          context
                              .read<RegisterCubit>()
                              .setFields("address", value);
                        }
                      },
                    ));
              }),
              const SizedBox(height: 20),
              ElevatedButton(
                onPressed: () {
                  if (_createComplexFormKey.currentState!.validate()) {
                    context.read<RegisterCubit>().setCreateComplexFields();
                  }
                },
                child:
                    const Text("Create", style: TextStyle(color: Colors.white)),
                style: ElevatedButton.styleFrom(
                    padding: const EdgeInsets.only(left: 30, right: 30),
                    shape: const StadiumBorder(),
                    elevation: 0,
                    primary: Theme.of(context).secondaryHeaderColor),
              )
            ],
          ),
        ));
  }
}
