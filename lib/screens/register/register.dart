import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:lodge/data/cubits/registerCubit.dart';
import 'package:lodge/data/models/register.dart';

class RegisterScreen extends StatelessWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(body: SafeArea(child: LayoutBuilder(
      builder: ((context, constraints) {
        return SingleChildScrollView(
            child: ConstrainedBox(
                constraints: BoxConstraints(
                  minHeight: constraints.maxHeight,
                  minWidth: constraints.maxWidth,
                ),
                child: IntrinsicHeight(
                  child: Column(children: [
                    const AppBarRegister(),
                    Container(
                      margin: const EdgeInsets.only(left: 50, right: 50),
                      child: const RegisterForm(),
                    ),
                    const Spacer(),
                    Padding(
                      padding: const EdgeInsets.only(bottom: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Text("Already have an account?"),
                          TextButton(
                              onPressed: () {
                                Navigator.pop(context);
                              },
                              child: Text("Sign in",
                                  style: TextStyle(
                                      color: Theme.of(context)
                                          .secondaryHeaderColor,
                                      fontWeight: FontWeight.bold)))
                        ],
                      ),
                    )
                  ]),
                )));
      }),
    )));
  }
}

class AppBarRegister extends StatelessWidget {
  const AppBarRegister({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        Container(
          height: 50,
        ),
        Container(
            padding: const EdgeInsets.only(left: 14),
            height: 50,
            width: MediaQuery.of(context).size.width,
            alignment: Alignment.centerLeft,
            child: IconButton(
                onPressed: () {
                  context.read<RegisterCubit>().resetState();
                  Navigator.pop(context);
                },
                highlightColor: Colors.transparent,
                splashColor: Colors.transparent,
                padding: EdgeInsets.zero,
                constraints: const BoxConstraints(),
                icon: Icon(Icons.close,
                    color: Theme.of(context).secondaryHeaderColor, size: 25))),
        Container(
            height: 50,
            alignment: Alignment.center,
            child: Text("Create User",
                style: GoogleFonts.mPlus1(
                    textStyle: TextStyle(
                        color: Theme.of(context).secondaryHeaderColor,
                        fontSize: 18,
                        fontWeight: FontWeight.bold))))
      ],
    );
  }
}

class RegisterForm extends StatefulWidget {
  const RegisterForm({Key? key}) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  // @override
  // void initState(){
  //   super.initState();
  // }

  final _RegisterFormKey = GlobalKey<FormState>();
  bool isAdmin = false;
  bool gender = false;

  void changeAdmin(bool change) {
    context.read<RegisterCubit>().setFields("admin", change);
    setState(() {
      isAdmin = change;
    });
  }

  void changeGender(nextGender) {
    context.read<RegisterCubit>().setFields("gender", nextGender);
    setState(() {
      gender = nextGender;
    });
  }

  static const textFields = ["Name", "Username", "Email"];

  @override
  Widget build(BuildContext context) {
    return Form(
        key: _RegisterFormKey,
        child: Column(
          children: <Widget>[
            ...textFields
                .map((e) => BlocBuilder<RegisterCubit, RegisterState>(
                      builder: (context, state) => TextFormField(
                          inputFormatters: [
                            LengthLimitingTextInputFormatter(40)
                          ],
                          autovalidateMode: AutovalidateMode.onUserInteraction,
                          onChanged: (value) {
                            context
                                .read<RegisterCubit>()
                                .setFields(e.toLowerCase(), value);
                          },
                          decoration: InputDecoration(
                            labelText: e,
                            labelStyle: const TextStyle(fontSize: 12),
                            // errorText: state.errors[e.toLowerCase()]
                          ),
                          validator: (value) {
                            return context
                                .read<RegisterCubit>()
                                .validation(e.toLowerCase(), value);
                          }),
                    ))
                .toList(),
            BlocBuilder<RegisterCubit, RegisterState>(
                builder: (context, state) => TextFormField(
                      inputFormatters: [LengthLimitingTextInputFormatter(12)],
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: const InputDecoration(
                        labelText: "Phone",
                        labelStyle: TextStyle(fontSize: 12),
                      ),
                      keyboardType: TextInputType.number,
                      onChanged: (value) {
                        context.read<RegisterCubit>().setFields("phone", value);
                      },
                      validator: (value) {
                        return context
                            .read<RegisterCubit>()
                            .validation("phone", value);
                      },
                    )),
            const SizedBox(height: 15),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Text("Gender",
                    style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6))),
                const SizedBox(width: 40),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: gender
                          ? Colors.white
                          : Theme.of(context).secondaryHeaderColor),
                  onPressed: (() => changeGender(false)),
                  child: Text("Male",
                      style: TextStyle(
                          color: gender
                              ? Theme.of(context).secondaryHeaderColor
                              : Colors.white)),
                ),
                const SizedBox(width: 10),
                ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      elevation: 0,
                      primary: gender
                          ? Theme.of(context).secondaryHeaderColor
                          : Colors.transparent),
                  onPressed: (() => changeGender(true)),
                  child: Text("Female",
                      style: TextStyle(
                          color: gender
                              ? Colors.white
                              : Theme.of(context).secondaryHeaderColor)),
                )
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: [
                const Text("Are you an admin",
                    style: TextStyle(color: Color.fromRGBO(0, 0, 0, 0.6))),
                Switch(
                    value: isAdmin,
                    onChanged: changeAdmin,
                    activeColor: Theme.of(context).secondaryHeaderColor),
              ],
            ),
            BlocBuilder<RegisterCubit, RegisterState>(
                builder: (context, state) => TextFormField(
                    inputFormatters: [LengthLimitingTextInputFormatter(40)],
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    decoration: const InputDecoration(
                      labelText: "Password",
                      labelStyle: TextStyle(fontSize: 12),
                    ),
                    obscureText: true,
                    enableSuggestions: false,
                    autocorrect: false,
                    onChanged: (value) {
                      context
                          .read<RegisterCubit>()
                          .setFields("password", value);
                    },
                    validator: (value) {
                      return context
                          .read<RegisterCubit>()
                          .validation("password", value);
                    })),
            BlocBuilder<RegisterCubit, RegisterState>(
                builder: (context, state) => TextFormField(
                      inputFormatters: [LengthLimitingTextInputFormatter(40)],
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      decoration: const InputDecoration(
                        labelText: "Confirm Password",
                        labelStyle: TextStyle(fontSize: 12),
                      ),
                      obscureText: true,
                      enableSuggestions: false,
                      autocorrect: false,
                      onChanged: (value) {
                        context
                            .read<RegisterCubit>()
                            .setFields("passwordConfirm", value);
                      },
                      validator: (value) {
                        return context
                            .read<RegisterCubit>()
                            .validation("passwordConfirm", value);
                      },
                    )),
            const SizedBox(height: 15),
            ElevatedButton(
              onPressed: () {
                if (_RegisterFormKey.currentState!.validate()) {
                  if (isAdmin) {
                    Navigator.pushNamed(context, '/registerComplex');
                  } else {
                    Navigator.pushNamed(context, '/registerUser');
                  }
                }
              },
              child:
                  const Text("Create", style: TextStyle(color: Colors.white)),
              style: ElevatedButton.styleFrom(
                  padding: const EdgeInsets.only(left: 30, right: 30),
                  shape: const StadiumBorder(),
                  elevation: 0,
                  primary: Theme.of(context).secondaryHeaderColor),
            )
          ],
        ));
  }
}
